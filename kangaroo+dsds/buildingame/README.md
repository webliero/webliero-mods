# BUILDING GAME

This mod is a building game (you can create a map in-game with it).

In this repository you can find the "standard" version of this mod (there is also a special "extended" version of this mod developed by dsds; you can find it [here](https://gitlab.com/sylvodsds/builder-mod)).

You can find some WebLiero maps created with Builder Mod [here](https://gitlab.com/sylvodsds/webliero-builder-maps).

# CHANGELOG

## v0.7.23

* fix "messing colours in bricks" bug
* set better x/y offset for bricks
* improve CONCRETE and SUPER PENCIL
* small change in LINE MARKER (0 reload time)
* replace unnecessary nObjects

## v0.7.22

* improve ERASING MISSILE ALT and SUPER PENCIL again
* full code review (remove last unnecessary object)

## v0.7.21

* improve ERASING MISSILE ALT and SUPER PENCIL
* DIRT ERASER: missile type weapon drawing background over dirt only
* GOO ROCK ALT: older version of this weapon added back as an alternative version
* clean code again (removed unnecessary objects)
* miscellaneous fixes and changes

## v0.7.20

* ERASING MISSILE ALT: missile type weapon drawing background over solid rock and dirt
* code cleaned (removed unnecessary objects)

## v0.7.18

* CONCRETE: creates small rocks which are drawn on the map
* GIDER: creates line made out of green dirt
* GIRDER ALT: creates line made out of rock
* SUPER PENCIL: missile type weapon drawing rock over all types of material

## v0.7.13

* BIG GIBBER: covers big area with worm gibs
* GIB FOUNTAIN ALT: shoots some worm gibs which are drawn on the map
* ROCK CUTTER: erases all types of materials
* ROCK CUTTER ALT: erases all types of materials in different way

## v0.7.10

* BLOOD FOUNTAIN: shoots blood particles
* ERASING MISSILE: missile type weapon drawing background over all types of material
* GIB FOUNTAIN: shoots some worm gibs which disappear on ground touch
* SMALL DIRTY: covers small area with dirt

## v0.7.5

* BIG DIRTY: covers big area with dirt
* DIRT FLAMER & DIRT FLAMER GREEN: creates some (green) dirt
* RUBBER ERASER ALT: short-range weapon drawing background over every type of material except for special rock
* DIRTY MISSILE: missile type weapon drawing dirt over background

## v0.7.1

* SPRAY: FOR RYSIEK with no delay
* PAINT BRUSH: missile type weapon drawing rock over every type of material

## v0.6.6

* FOR RYSIEK: dot drawing weapon for parkur (with solid rock)
* LINE MARKER: drawing straight lines of solid rock

## v0.5.0

* PENCIL: missile type weapon drawing rock over background and dirt
(useful to clear out dirt "holes")

## v0.4.1

* FLAMING ROCKER THAT SUCKS: flaming type weapon to print rock (color idx 19), mostly works vertically (from a pixel bellow or above), kinda useful to make walls and towers, or fill evenly with rock 

## v0.4.0

* DIRT FILLER: new missile type weapon drawing rock (color idx 19) over dirt and only dirt!
(useful to clear out dirt "holes")

## v0.3.0

* new building bricks:
  TETRIS ANGLE BLUE
  TETRIS ANGLE GREEN
  TETRIS ANGLE GREY
  TETRIS ANGLE RED
  TETRIS SQUARE GREY
  TETRIS TEEWEE BLUE
  TETRIS TEEWEE GREEN


## v0.2.1

no new weapons, but huge improvement on usability of bricks thanks to Szarnywirk's suggestions @ https://discord.com/channels/435544640599490560/515507463735214091/782706688725745724

bricks don't require a rock pixels to be placed since a small dirt effect is used to capture them in place

## v0.1.1

* GOO ROCK: new "goo like rock utility weapon", can be used to "fill" some holes, add rock pixels on the ground when it got completely cleared, or make drops of rock anywhere

## v0.0.1

first version, adds a few brick with different colors, a square & a boulder, and a few more "tetrises" including some to write "LIERO" in tetris bricks

bricks can be placed on "at least" a pixel of rock, else they won't be printed on the map

* building bricks:
  SQUARE
  BLUE BRICK
  GREEN BRICK
  GREY BRICK
  RED BRICK
  BOULDER
  TETRIS LEFT
  TETRIS RIGHT
  TETRIS VERTICAL
  TETRIS HORIZONTAL
  TETRIS E
  TETRIS R
  TETRIS O
  TETRIS BIG L

* utilities:
  RUBBER ERASER: used to clear small bits of rock or dirt
  FLAMING ERASER: used to clear wider parts of rock or dirt
  SUICIDE: useful whenever you get stuck
