XMAS Mod 2020

based on "Liero Promode Re-Revisted by Scharnvirk-Jerac" version 1.3.1

this mods is mainly a fun xmas theme sprite change

adds  weapons:
`DEADLY GIFT`
`CHRISTMAS TREE` (this one is both an healing & offensive weapon!)

changes weapons:
`SPIKEBALLS` to `SNOWSFLAKES`
`TUPOLEV` to `SANTA CLAUS`
`THROW KNIFE` to `SNOWBALLS`
