# Def Revisited (1v1 version) by Kangur

**Def Revisited (1v1 version)** - a long time ago (in a galaxy far, far away... :P), there was a nice guy called Daro who was a big fan of basro's game called GUSANOS (an epic Liero clone). Daro wanted to "port" default mod from gusanos into Webliero - and that's how he created his [famous Webliero "def" mod](https://gitlab.com/webliero/webliero-mods/-/tree/master/def).

So, as the name suggests, my aim was to "re-visit" this mod and revise some of its weapons, in order to make this mod more suitable for Webliero, especially for duels. The mod was made using sprites taken from liero promode final mod (you can also download the sprite file from this repository - file "sprites.wlsprt"). Comparing to original Daro's def mode, the changes include:

a) main "physics" parameters of the gameplay (part "constans" - I used liero promode rerevisited original settings);

b) weapon changes (see the list of weapons with their description below).

Generally, the mod contains 3 types of weapons:

a) attack weapons (PANZERFAUST, RAILGUN, SOLAR FLAME, MISSILE, ZIMM);

b) finishers (PLASMA SHOTGUN, MACHINE GUN, LASER CUTTER, FLAMETHROWER);

c) defense weapons (DIRT SHIELD, MAGNETIC FIELD);

d) bombs (UZUMAKI, NAPALM, GRENADE, MORTAR).

The main ideas of the mod was:

a) to make some weapons passing through walls (in order to prevent defensive playstyle - u cannot now just hide behind the wall since you can get killed);

b) to make very powerful bombs (mostly instagib) and pretty powerful (but not instagib) attack weapons;

c) to make two different types of defense weapons which work in different way (i.e. one shield protects from some weapons, and the second shield protects from another weapons).

List of all weapons and their description is below:

**PANZERFAUST** - it fires a very fast bullet which deals a lot of damage on a very high range. However, the bullet doesn't explode on worm touch (it passes through worm) but only on ground hit. This weapon is modelled on EARTHSHAKER taken from Kami's "dtk 1v1" mod (which in turn was based on ARMOUR-PIERCING RPG from jerrac's "Gunz!" mod).

**SOLAR FLAME** - it shoots flames which can pass through walls and deal a lot of damage to the enemy. The idea to make such weapon was DESOLATOR from Kami's "dtk 1v1" mod, however it was remodelled a lot.

**RAILGUN** - it fires a very fast, golden beam of energy that deals a lot of damage on worm hit. Taken from my "Roo Fun Mod", but buffed a little bit (more dmg added).

**MACHINE GUN** - it fires a series of very fast projectiles. It has large amount of bullets in the magazine. A very useful weapon for finishing off the opponent when his hp level is low.

**FLAMETHROWER** - this weapon was taken directly from Liero Promode ReRevisited (version 1.3.2), but it was buffed a lot (more dmg added, shorter loading time).

**PLASMA SHOTGUN** - it works similar to AUTO SHOTGUN taken form Liero Promode ReRevisited (version 1.3.2), but it was buffed a little bit + some visual effects added.

**LASER CUTTER** - it works similar to normal LASER from Liero Promode Final, but the bullets can pass through walls, the laser beam is golden-coloured and its range is limited. A very useful weapon for finishing off the opponent when his hp level is low.

**GRENADE** - this weapon was taken directly from Liero Promode ReRevisited (version 1.3.2), but it was buffed a little bit (more dmg added).

**UZUMAKI** - this weapon was taken directly from Kami's "dtk 1v1" mod (which in turn was based on Daro's "def" mod), but slightly nerfed.

**NAPALM** - this weapon was taken directly from Liero Promode Final, but it was buffed a lot (more dmg added + it now explodes on worm touch too).

**DIRT SHIELD** -this weapon was taken directly from Kami's "dtk 1v1" mod (SHIELD), but slightly changed to adjust it to this mod.

**MAGNETIC FIELD** - this weapon was taken directly from my "Roo Fun Mod" (ANDRE'S MAGNETIC SHIELD), but slightly changed to adjust it to this mod.

**MISSILE** - this weapon was taken directly from Liero Promode ReRevisited (version 1.3.2), but it was buffed a lot (more dmg added, less reload time, more ammo added).

**MORTAR** - this weapon was taken directly from Liero Promode ReRevisited (version 1.3.2), but it was changed a little bit to adjust it to this mod.

**ZIMM** - this weapon was taken directly from Liero Promode ReRevisited (version 1.3.2), but it was buffed a lot (more dmg added, less reload time, more bullet speed, more time to explode, less delay).

_-- current version of the mod: 1.2 --_

**_roo_**
