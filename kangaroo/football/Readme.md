# Liero Football Game by Kangur

Did you know that the creator of webliero (basro a.k.a. webler) is also known for making a very cool browser-based online multiplayer soccer game called [Haxball](https://www.haxball.com/)? This game is very popular in the web. Anyway, what if you could "merge" those two (webliero and haxball) into one game? I mean - what if you can play soccer in webliero rather than just kill other worms?

If you like this crazy idea, then this small mod (called simply "Liero Football Game") is something you are looking for (welp, maybe "football game" is not the best name for the mod since worms don't have legs, but I don't care) :P. So, in this mod you basically play soccer - that is, there are only 3 "weapons" in this mod:

1. **BALL**
2. **ACID FAN**
3. **BALL RELEASE**

The mod is generally based on game physics taken from Liero Promode ReRevisited (except for one thing - the worm gravity is set to 0 in order to allow players to move freely on the map). The mod is also using custom sprites (generally based on Liero Promode ReRevisited too with some changes and addidtions) which you can also find here. The purpose of the game is to use ACID FAN to move the BALL and try to put it into the goal. Sometimes the BALL can get stuck in the walls; if such thing happens, you can use BALL RELEASE to free the BALL.

The mod is pretty useless though without a room script using special tricks to detect ball position on the map (and count the score when it is inside the goal) - which is afaik possible only using WebLiero Extended :/ that's why the mod is unfinished and only beta-version. I will maybe develop the mod more in the future, e.g. by adding more "realistic weapons" (i.e. kick, sideslip, pass, overhead kick etc.).

**_roo_**