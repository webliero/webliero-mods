# Liero 1.33 Crossbow mod by Larcelo (version 2.0)

Here you can find webliero-fixed version of Larcelo's Liero 1.33 Crossbow Mod, in which you have 18 weapons but 14 of them are different types of crossbow.

The mod was originally created as a Liero 1.33 TC, so that some things had to be changed to adapt it to Webliero, that is:

1. classic Liero 1.33 weapons (RIFLE, SPIKEBALLS etc.) were removed;
2. weapon names were translated into English (originallly the names were in Polish).
