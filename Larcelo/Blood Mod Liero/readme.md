# Liero 1.33 Blood Mod Liero by Larcelo (version 2.0)

Here you can find webliero-fixed version of Larcelo's Liero 1.33 Blood Mod Liero mod.

The mod was originally created as a Liero 1.33 TC, so that some things had to be changed to adapt it to Webliero, that is:

1. weapon OSLONA ZIEMIA renamed to DIRT SHIELD and RADIOAKTIVES to RADIOACTIVES;
2. changed timeToExplo in wObject of weapon BLACK HOLE from 5000 to 500 and bloodOnHit from 255 to 0;
3. changed timeToExplo in wObject of weapon BLACK HOLE from 7000 to 500;
4. RAILGUN was remodelled a lot (changed parameters: parts, hitDamage, bloodOnHit, startFrame, splinterAmount and partTrailDelay).
