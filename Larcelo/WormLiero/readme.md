# Liero 1.33 WormLiero mod by Larcelo (version 1.15)

Here you can find webliero-fixed version of Larcelo's Liero 1.33 WormLiero mod, inspired by a famous computer game Worms Armageddon.

The mod was originally created as a Liero 1.33 TC, so that some things had to be changed to adapt it to Webliero, that is:

1. weapon ARMAGEDDON was removed (because it caused the game crash due to too many sobjects with high dmg);
2. weapon names were translated into English (originallly the names were in Polish);
3. changed timeToExplo in wObject of weapon THRASH CAN (originally: SMIETNIK) from 7000 to 700.
