# Liero 1.33 Food Fight mod by Larcelo (version 1.0)

Here you can find webliero-fixed version of Larcelo's Liero 1.33 Food Fight mod.

The mod was originally created as a Liero 1.33 TC, so that some things had to be changed to adapt it to Webliero, that is:

1) some weapons were removed (the ones which had "---" in their names - since they were banned anyway in original TC);
2) weapon names were translated into English (originally the names were in Polish).
