# Liero 1.33 Tibia Mod by Larcelo (version 1.3)

Here you can find webliero-fixed version of Larcelo's Liero 1.33 Tibia Mod.

The mod was originally created as a Liero 1.33 TC, so that some things had to be changed to adapt it to Webliero, that is:

1. weapon CIP DEAD was removed (because it was too powerful and caused game crash);
2. longer reload time set for almost all weapons (it was absurdly short);
3. weapon LEVITATE fixed;
4. lower timeToExplo in ENERGY FIELD and FIRE FIELD;
5. lower delay time between shots in KNIGHT ATTACK;
6. fixed BERSERK and FIERCEBERSERK;
7. fixed all healing weapons.
