# TANK Mod (by Foobaz (skizzord) + tank sprites by Kangur)

## v1.3-experimental
(misc improvements)

## v1.1-experimental
* BUBBLE TRAPS

## v1
(misc improvements)

## v1alpha7c

* REPAIR POD Deploys a hovering pod that has a delay before emitting healing particles
       before running out of energy and crashing to the ground.
       Heals, but has a long delay before reload and the user needs to remain relatively
       stationary while healing.
* TRACTOR BEAM: An arcing, bouncing beam affected by gravity that pulls and deals slight damage to
        anything it touches.  Also destroys dirt.  Useful for clearing out bunkers, dirt, pulling back would-be
        escapees, and pulling foes in for a one-two punch when combined with switching weapons rapidly.  Can
        also be used to quickly move towards a surface by reflected the beam back towards yourself and taking some
        damage.    

## v1alpha6
(misc improvements)

## v1alpha5
* WALLHAX Wall piercing projectile that turns into bombs
* MISSILE LAUNCHER

## v1alpha3

* GRAVITY WELL A short range projectile with no gravity that sucks players in and deals damage over a short time period.
       Essentially a hovercrack with shorter lived projectiles and negative recoil.  Excellent at close ranges against stationary
       or slow moving foes.
* DRILL MISSILE A controllable missile that falls faster than it rises and can also drill through dirt.
       Releases nuke particles after detonating.  Great for surprising tunnelers.
* DRAGON'S BREATH Slow moving flamethrower with knockback characteristics of fan against players.
       Flames are affected by gravity and will stay on the floor until the projectile snuffs out.
       Useful against walls or shot into tunnels
* BOUNCING BETTY Bouncy projectile that explodes after some time, unleashing a large amount of
       cluster bomblets.  Useful for flushing out turtling enemies behind a hill or below you
* BALLOON MINE Upwards floating projectile that's like a proxy mine with a mortar blast.
       Has seven shots prior to reload.  Useful for area denial against foes above you.
* MINIGUN Shoots large numbers of bullets in extremely rapid bursts.  Short reload.
         Exposed enemies won't live long against this one
* CANISTER SHOT Shoots a large number of spikeballs which can skip along the floor or some walls
* MEGA BEAM technically a slow moving bouncy incendiator with 
        an extremely slow start time but which accelerates rapidly.
        Useful for clearing out large swaths, but make sure the backblast area is
        clear or you'll kill yourself
* ORBITAL STRIKE Laser guided strike which sends projectiles upwards, which later spawn
       bombs spawning downwards after some delay.  Laser hits also deal some damage. 
       Good for flushing out opponents and raining down fire on map edges.
* JAVELIN Set of two steerable missile which can be shot in rapid succession and eject a damaging payload after flying
            a short time.  Good for dumping large amounts of damage around corners and damaging enemies behind thin walls or floors
* ARMOR PIERCER Extremely fast sniper projectile with long range that can blast through large amounts of dirt
                  as well as deal explosive damage against enemies behind indestructable barriers.
                  Has a secondary explosive effect which is useful for plinking enemies or if you miss.
                  Must be used judiciously as it has a long reload, other weapons can often deal more DPS,
                  and is affected by player velocity, making it harder to aim while moving quickly.
* FORTIFY Makes a barrier of dirt in a random fashion, offering a barrier of protection.  For a short time
            will also spawn regenerating greenball dirt, offering extra protection against some weapons and
            making it harder to move if trapped inside it.  Useful for blocking minigun, canister shot, and
            mega beam, among others.
* JETPACK A jetpack which allows for superior mobility compared with the shortened ninja rope.  
            Good for gaining high ground and getting out of disadvantageous situations, but not really meant for dealing damage :)
