el clásico moderno
==================

A modernized Liero mod based on 1.33

- High mobility
- Original 1.33 weapons with liberal modifications
- Delay-based weapons (zero reload)
- More elaborate particle effects
- No drawOnMap from shells or gibs
- A cooler palette

Created by wgetch
