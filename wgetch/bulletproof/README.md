bulletproof
===========

A simple combat mod with five weapons:

DMR - Designated marksman rifle
PDW - Personal defense weapon
SHOT - Tactical shotgun
FRAG - Fragmentation grenade
SIDEARM - Pistol (no reload)

The primary goals for this mod are:
 - Minimalist and ballistic-focused
 - Should be playable at any damage multiplier
 - Should be playable at any loading times

The secondary goal is that it should also work with the following "tactical" configuration (optional):
 - All weapons banned except SIDEARM
 - Weapon crate bonuses enabled
 - Loading times set to 999

Created by wgetch for Wurmjam 2024
