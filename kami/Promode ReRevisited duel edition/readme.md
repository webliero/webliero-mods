## Promode ReRevisited duel edition (AKA RevX)

Kami's version of Promode ReRevisited mod, reworked to make it suitable for duels (1v1 games).

# version 0.033

# CHANGELOG

|  NEW SETTINGS         |      ORIGINAL SETTINGS
|---------------------- | ------------------------
|***weapon***
|name: "AUTO SHOTGUN"   |    name: "AUTO SHOTGUN"
|delay: 17,             |    delay: 28,
|***wobject***
|hitDamage: 1.4,        |    hitDamage: 1,
|***weapon***
|name: "CHAINGUN"       |    name: "CHAINGUN"
|bulletSpeed: 4.2,      |    bulletSpeed: 4,
|ammo: 25,              |    ammo: 50,
|loadingTime: 310,      |    loadingTime: 299,
|***wobject***
|hitDamage: 6,          |    hitDamage: 4,
|blowAway: 0,           |    blowAway: 0.05,
|***weapon***
|name: "LIGHTNING GUN"  |    name: "LIGHTNING GUN"
|ammo: 100,             |    ammo: 60,
|***wobject***
|timeToExplo: 170,      |    timeToExplo: 140,
|hitDamage: 2.2,        |    hitDamage: 1.8,
|blowAway: 0.3,         |    blowAway: 0.12,
|***weapon***
|name: "GRN LAUNCHER"   |    name: "GRN LAUNCHER"
|***wobject***
|detectDistance: 2,     |    detectDistance: 1,
|bounce: 0.7,           |    bounce: 0.4,
|hitDamage: 20,         |    hitDamage: 4,
|timeToExplo: 220,      |    timeToExplo: 140,
|***weapon***           |
|name: "GRENADE"        |    name: "GRENADE",
|loadingTime: 145,      |    loadingTime: 200,
|***nobject***
|timeToExplo: 19,       |    timeToExplo: 15,
|***weapon***           |
|name: "DOOMSDAY"       |    name: "DOOMSDAY",
|parts: 4,              |    parts: 2,
|bulletSpeed: 2.5,      |    bulletSpeed: 2.1,
|ammo: 6,               |    ammo: 8,
|delay: 13,             |    delay: 11,
|loadingTime: 440,      |    loadingTime: 400,
|bulletSpeedInherit: 0.17619047619047616, |    bulletSpeedInherit: 0.47619047619047616,
|***wobject***
|distribution: 0.120517578125,       |    distribution: 0.030517578125,
|hitDamage: 2,          |    hitDamage: 0,
|***weapon***
|name: "FLAMER"         |    name: "FLAMER"
|ammo: 60,              |    ammo: 50,
|***wobject***
|timeToExplo: 28,       |    timeToExplo: 25,
|hitDamage: 2,          |    hitDamage: 1,
|***weapon***
|name: "CLUSTER POD"    |    name: "CLUSTER POD"
|parts: 19,             |    parts: 20,
|ammo: 2,               |    ammo: 1,
|delay: 40,             |    delay: 0,
|loadingTime: 440,      |    loadingTime: 400,
|bulletSpeedInherit: 0.3142857142857143,      |    bulletSpeedInherit: 0.7142857142857143, 
|***wobject***
|hitDamage: 1,          |    hitDamage: 0,
|gravity: 0.01783642578125,  |    gravity: 0.01983642578125,
|timeToExplo: 12,       |    timeToExplo: 40,
|***weapon***
|name: "TUPOLEV"        |    name: "TUPOLEV"
|bulletSpeed: 1.7,      |    bulletSpeed: 2,
|bulletSpeedInherit: 0.2, |  bulletSpeedInherit: 0.5,
|loadingTime: 460,      |    loadingTime: 400,
|***wobject***
|timeToExplo: 0,        |    timeToExplo: 105,
|partTrailDelay: 6,     |    partTrailDelay: 10,
|detectDistance: 2,     |    detectDistance: 1,
|***nobject***
|gravity: 0.040517578125,    |    gravity: 0.030517578125,
|***weapon***
|name: "SCATTERGUN"     |    name: "SCATTERGUN"
|bulletSpeed: 2.9,      |    bulletSpeed: 2.5,
|***wobject***
|hitDamage: 5,          |    hitDamage: 3,
|***weapon***
|name: "MINIGUN"        |    name: "MINIGUN"
|ammo: 70,              |    ammo: 80,
|loadingTime: 460,      |    loadingTime: 360,
|***wobject***
|hitDamage: 4,          |    hitDamage: 2,
|***weapon***
|name: "DARTGUN"        |    name: "DARTGUN"
|loadingTime: 360,      |    loadingTime: 217,
|ammo: 7,               |    ammo: 6,
|delay: 9,              |    delay: 18,
|***wobject***
|timetoExplo: 1100,     |    timeToExplo: 300,
|timeToExploV: 20,      |    timeToExploV: 0,
|blowAway: 0.5,         |    blowAway: 0.3,
|distribution: 0.03288818359375,         |    distribution: 0.02288818359375,
|***weapon***
|name: "LASER"          |    name: "LASER"
|***wobject***
|hitDamage: 1.4,        |    hitDamage: 1,
|***weapon***
|"MINI NUKE"            |    name: "MINI NUKE"
|***wobject***
|splinterAmount: 12,    |    splinterAmount: 8,
|***nobject***
|detectDistance: 2,     |    detectDistance: 1,
|speed: 0,              |    speed: 2,
|speedV: 0,             |    speedV: 1.2,
|distribution: 0.425,   |    distribution: 0.25,
|timeToExplo: 200,      |    timeToExplo: 80,
|timeToExploV: 30,      |    timeToExploV: 20,
|***weapon***
|name: "SPIKEBALLS"     |    name: "SPIKEBALLS",
|parts: 8,              |    parts: 7,
|ammo: 2,               |    ammo: 1,
|delay: 70,             |    delay: 0,
|loadingTime: 460,      |    loadingTime: 400,
|bulletSpeedInherit: 0.7090909090909091,  |   bulletSpeedInherit: 0.9090909090909091,
|***wobject***
|hitDamage: 13,         |    hitDamage: 9,
|timeToExplo: 860,      |    timeToExplo: 230,
|bounce: 0.6,           |    bounce: 0.5, 
|detectDistance: 1,     |    detectDistance: 2,
|***weapon***
|name: "ZIMM"           |    name: "ZIMM"
|bulletSpeed: 3.1,      |    bulletSpeed: 3,
|bulletSpeedInherit: 0.1, |  bulletSpeedInherit: 0.3333333333333333,
|delay: 60,             |    delay: 90,
|***wobject***
|hitDamage: 63,         |    hitDamage: 49,
|timeToExplo: 380,      |    timeToExplo: 140,
|***weapon***
|name: "THROW KNIFE"    |    name: "THROW KNIFE"
|playReloadSound: false,|    playReloadSound: true,
|***wobject***
|hitDamage: 39,         |    hitDamage: 24,
|***weapon***
|name: "GAUSS GUN"      |    name: "GAUSS GUN"
|bulletSpeed: 2.3,      |    bulletSpeed: 2,
|***wobject***
|detectDistance: 2,     |    detectDistance: 1,
|hitDamage: 2,          |    hitDamage: 1,
|***weapon***
|name: "FLAK CANNON"    |    name: "FLAK CANNON"
|bulletSpeed: 5.3,      |    bulletSpeed: 5.1,
|delay: 15,             |    delay: 20,
|***wobject***
|hitDamage: 18,         |    hitDamage: 15,
|***weapon***
|name: "PROXY MINE"     |    name: "PROXY MINE"
|***wobject***
|blowAway: 0.12,        |    blowAway: 0.22,
|***weapon***
|name: "SOLAR SCORCH"   |    name: "SOLAR SCORCH"
|ammo: 20,              |    ammo: 17,
|***wobject***
|hitDamage: 2.1,        |    hitDamage: 2,
|blowAway: 0.055,       |    blowaway: 0.015,
|***weapon***
|name: "CHIQUITA GUN"   |    name: "CHIQUITA GUN"
|parts: 5,              |    parts: 4,
|loadingTime: 420,      |    loadingTime: 400,
|***weapon***
|name: "ACID FAN"       |    name: "ACID FAN"
|***wobject***
|hitDamage: 1.5,        |    hitDamage: 1,
|***weapon***
|name: "BAZOOKA"        |    name: "BAZOOKA"
|ammo: 4,               |    ammo: 3,
|delay: 50,             |    delay: 60,
|bulletSpeed: 2.7,      |    bulletSpeed: 2.6,
|***weapon***
|name: "INCENDIATOR"    |    name: "INCENDIATOR",
|***wobject***
|timeToExplo: 180,      |    timeToExplo: 70,
|***weapon***
|name: "MISSILE"        |    name: "MISSILE"
|***wobject***
|detectDistance: 2,     |    detectDistance: 1,
|***weapon***
|name: "MORTAR"         |    name: "MORTAR"
|delay: 20,             |    delay: 50,
|loadingTime: 265,      |    loadingTime: 300,
|***wobject***
|blowAway: 0.15,        |    blowAway: 0.1,
|***weapon***
|name: "RIFLE"          |    name: "RIFLE"
|***wobject***
|hitDamage: 34,         |    hitDamage: 32,
|gravity: 0,            |    gravity: 0.00030517578125,
|***weapon***
|name: "WINCHESTER"     |    name: "WINCHESTER"
|***wobject***
|gravity: 0,            |    gravity: 0.00030517578125,
|***weapon***
|added CRACKFIELD, CHIQUITA BOMB, TUPOLEV BOMB, BIG NUKE, ARPA, GREENBALL, DIRTBALL  |   - 
