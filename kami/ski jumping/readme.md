# skijumping

by Kami

an experimental mod intended to simulate ski jumping:

* worms cannot move using left/right arrow keys
* no weapons but ACID FAN (used to move the worm)
* crosshair can rotate in more than 360 angle (to simulate "balancing in the air")

recommended to play only on WebLiero Extended with a special room script.