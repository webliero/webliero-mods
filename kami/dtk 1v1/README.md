# dtk 1v1 mod

by Kami

fast physics, skill oriented, mod with only 7 weapons, aimed to be used for duels

sprites are the same as Promode ReRevisited

# CHANGELOG

## 0.41

* WORLD ENDER removed
* some changes in NUKE
* lowered delay time in all weapons from 420 to 360
* code clean
* credits

## 0.4

* various changes to physics (constans section)
* MORTAR changed to EARTHSHAKER (+ some changes)
* BAZOOKA replaced with MISSILE
* MINI NUKE renamed to NUKE (+ some changes)
* DESOLATOR and SHIELD rebalanced
* new weapons added: UZUMAKI, ACCELERATE and WORLD ENDER
* NO WEAPON removed


## 0.1

* various changes to physics (constans section)
* weapons: MORTAR, SHIELD, BAZOOKA, MINI NUKE, DESOLATOR
* NO WEAPON (useless, intended to fix an issue with random weapons when only 5 weapons are in the mod)
