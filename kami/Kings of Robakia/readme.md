# Kings of Robakia

by Kami

an interesting and experimental mod based on Jerac's Magic Playgroud WebLiero Mod, intended to be RPG-like (with different versions & "power levels" of weapons).

recommended settings: only 1st level weapons are available at the beginning (all with "LV I" in their names), all others are banned & can be found in weapon crates.