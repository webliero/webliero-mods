# ktk 1v1 mod

by Kami

fast physics, skill oriented, mod with only 16 weapons, aimed to be used for duels

# CHANGELOG

## 0.2

* added new weapon: TAUNT
* buff ACID FAN a little bit (more ammo)
* nerf FLAME STRIKE a little bit (longer reload time)
* buff RAILGUN (more ammo)
* rebalance MORTAR (longer reload time but changed gravity)
* rebalance MACHINEGUN (less damage but lower distribution)
* rebalance LIGHTSABER (less damage but less blowAway and bounce -1)

## 0.1

* weapons: ACID FAN, CRACKFIELD, FLAME STRIKE, HEAVY CANNON, HELLRAIDER, LASER, LIGHTSABER, MACHINEGUN, MORTAR, RAILGUN, ROCKET LAUNCHER, SHOTGUN, SOLAR FLAME, TACTICAL SQUARE, UZI