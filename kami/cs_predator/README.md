# Counterstrike Predator

a mod counterstrike based mod, where the worm is invisible (well.. black :p)

### suggested map pool

```
[
  "神風/cs/cs_fda.png",
  "神風/cs/cs_ash.png",
  "神風/cs/cs_dm1.png",
  "神風/cs/cs_vault_b.png"
]
```


### suggested rules

ideally the players should have an empty name so they don't give their position away

```
{
  "bonusDrops": "healthAndWeapons",
  "bonusSpawnFrequency": 30,
  "damageMultiplier": 1,
  "expandLevel": false,
  "forceRandomizeWeapons": true,
  "gameMode": "dm",
  "levelPool": "rng",
  "loadingTimes": 0.4,
  "lockWeaponsDuringMatch": false,
  "maxDuplicateWeapons": 0,
  "reloadWeaponsOnSpawn": true,
  "respawnDelay": 3,
  "scoreLimit": 35,
  "timeLimit": 10,
  "weaponChangeDelay": 0
}
```
