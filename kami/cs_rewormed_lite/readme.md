# CS Rewormed Lite

by Kami

based on CS Rewormed but removing some weapons

# CHANGELOG

## 0.29

### COMMON WEAPONS:

* __H&K G3/SG-1__
* __MACHINEPISTOL__ 
* __MAC-10__
* __MINI GUN__
* __GLOCK BURSTFIRE__
* __FLAMETHROWER__
* __NIGHT HAWK__
* __KALASHNIKOV__
* __DARUDE SANDSTORM__
* __COLT-CARBINE__
* __UMP__
* __HELLFIREROCKETS__
* __USAS-12__
* __PARA M249__
* __RPG-7__
* __APSHOCKCANNON__
* __NAILGUN__
* __P-90__
* __STEYR SCOUT__
* __FLAKCANNON__
* __SHOCKGRENADE__

### SPECIAL WEAPONS:

* __FLAMETHROWER__
* __GAUSSGUN__
* __BLACK BETTY__
