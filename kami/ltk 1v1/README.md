### LTK 1v1

derived from DTK 1v1, LTK 1v1 has different (more conventional) physics and introduces new weapons  
the mod is a faced paced duelling mod using delay property instead of loadingTime so your weapons "reload delay" isn't impacted by weapon switching

## version 0.11

- SHIELD (utility weapon)
- ACCELERATE (utility weapon)
- EARTHSHAKER
- DESOLATOR
- MISSILE
- NUKE
- SSG

