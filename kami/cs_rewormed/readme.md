# CS Rewormed

by Kami

sprites are the same as CS Liero 2.2b

# CHANGELOG

## 0.32c

misc balancing, new sprites, new weapons..

### NEW COMMON WEAPON:

* MERR-SONN PLX-2M

### COMMON WEAPONS BALANCING:

* DUAL 96G BERETTAS
* DESERT EAGLE
* C4
* HELLFIREROCKETS
* VIRUS

## 0.32b

misc balancing, new sprites, lots of weapon renaming..

### NEW COMMON WEAPON:

* AGS-17

### NEW SPECIAL WEAPON:

* NUKA-COLA

## 0.32a

misc balancing, new sprites..

### NEW SPECIAL WEAPON

* GADGET

## 0.32

### NEW SPRITES

* __FLAMETHROWER__ => __M9A1-7 FLAMETHROWER__


### NEW COMMON WEAPONS:

* FLAMMENWERFER 41
* GShG-7.62
* SIG P228
* MOLOTOV COCKTAIL

### REMOVED:

* ARSINE
* FORCE FIELD
* ATOMIC☢ANNIE

## NEW SPECIAL WEAPON:

* CALTROPS
* STEALTH MINE
* FORCE FIELD


## 0.31c

### NEW SPRITES

### COMMON WEAPONS BALANCING:

* GLOCK18 BURSTFIRE
* NIGHT HAWK
* RHEINMETALL RMK30
* DUAL 96G BERETTAS
* RPO-A BUMBLEBEE
* SIG SG-550 SN
* STEYR AUG-77
* SHOCKGRENADE

### NEW COMMON WEAPONS:

* ARSINE

## SPECIAL WEAPON BALANCING:

* GLOCK18 BURSTFIRE
* BARRACUDA

## 0.31b

### NEW SPRITES

### COMMON WEAPONS:

* __STEYR TMP__ loadingTime (520 -> 335)
* __FORCE FIELD__ bulletSpeed (3.7 -> 4.2)
* __EXCIMER LASER__ ammo (75 -> 47), loadingTime (500 -> 580)
* __DUAL 96G BERETTA__ delay (7 -> 0), loadingTime (95 -> 100), bulletSpeedInherit (0.193030303030304 -> 0.194030303030304), detectDistrance (2 -> 3), wider distribution
* __RPO-A BUMBLEBEE__ bulletSpeed (2.33 -> 2.34), loadingTime (880 -> 685), remove leaveShells
* __RAGING BULL__ delay (58 -> 52)
* __STEYR AUG-77__ ammo (23 -> 21)
* __FORCE FIELD__ multSpeed (0.91 -> 0.93)
* __MOSSBERG-500__ detectDistance (2 -> 3), wider distribution, hitDamage (15 -> 14)
* __RHEINMETALL RMK30__ (10 -> 12)
* __SHOCKGRENADE__ detectDistance (2 -> 5)

### SPECIAL WEAPONS:

* __SD-899 A1 VIRUS SRIFLE__ new weapon
* __FAMAS F1__ new weapon
* __XM214 MINIGUN__ hitDamage (18->22)


## 0.31a

### NEW SPRITES

### COMMON WEAPONS:

* __NAPALM__ ammo (4 -> 3)
* __GLOCK18 BURSTFIRE__ ammo (7 -> 8), delay (23 -> 21), objTrailDelay (0 -> 1)
* __CANNONBALL__ ammo (3 -> 2)
* __STEALTH MINE__ removed
* __M79__ loadingTime (250 -> 265)
* __TERROR MINE__ removed
* __DUAL 96G BERETTAS__ bulletSpeedInherit (0.253030303030304 -> 0.193030303030304), bullet gravity (0.00499728515625 -> 0.00399728515625)
* __MOSSBERG-500__ delay (31 -> 30)
* __RPO-A BUMBLEBEE__ bulletSpeed (2.3 -> 2.33), recoil (1.4 -> 1.5)
* __RPG__ detectDistance (2 -> 3)
* __SIG SG-550 SN__ hitDamage (99 -> 98)
* __USAS-12__ hitDamage (7.2 -> 7.3)
* __C4__ bullet (multSpeed 1 -> 0.92, bounce 0 -> 1)

### SPECIAL WEAPONS:

* __ATOMIC☢ANNIE__ new weapon
* __HITACHI GRENADE GENERATOR__ bulletSpeed (0 -> 0.001), bullet (multSpeed 1 -> 0, partTrailDelay 600 -> 500)

## 0.31

### NEW SPRITES

### COMMON WEAPONS:

* __TERROR MINE__ new weapon
* __C4__ new weapon
* __MK.III GRENADE__ ammo (3 -> 2), loadingTime (530 -> 540), splinterAmount (55 -> 66), nObject (detectDistance 8 -> 9, bounce 0 -> 0.9, explGround false)
* __HELLFIREROCKETS__ bullet blowAway (0.02 -> 0.032)
* __CANNONBALL__ bullet gravity (0.023 -> 0.021)
* __MOSSBERG-500__ hitDamage (13 -> 15)


### SPECIAL WEAPONS:

* __NAPALM__ new weapon

## 0.3d

### COMMON WEAPONS:

* __NAPALM__ finetuning
* __CANNONBALL__ bulletSpeed 2.9 -> 1.9
* __GAU-19/A__ new weapon
* __RPO-A BUMBLEBEE__ new weapon
* __MK.III GRENADE__ new weapon
* __SIG SG-550 SN__ bulletSpeed 3.3 -> 4

### SPECIAL WEAPONS:

* __CANNONBALL__ new weapon

## 0.3c

### COMMON WEAPONS:

* __NAPALM__ new weapon
* __M79__ loadingTime (290 -> 245)
* __M242 BUSHMASTER__ new weapon
* __MOSSBERG-500__ delay (33 -> 31)

## 0.3b

### COMMON WEAPONS:

* __M79__ new weapon
* __KALASHNIKOV__ renamed AK-47
* __M249 SAW__ ammo 100 -> 60
* __DUAL 96G BERETTAS__ loadingTime (100 -> 95)
* __MOSSBERG-500__ new weapon
* __NAILGUN__ delay 8 -> 7
* __RAGING BULL__ new weapon
* __BARRETT XM109__ bulletSpeed (2.6 -> 2.5), ammo (2 -> 3), delay (160 -> 125), loadingTime (700 -> 600)
* __STEYR AUG-77__ new weapon
* __XM214 MINIGUN__ sligthly more damage (hitdamage 15 -> 16)

### SPECIAL WEAPONS:

* __RPG-7__ renamed => V2 ROCKET LAUNCHER, ammo 4 -> 9, delay 12 -> 16
* __NAILGUN__ delay 9 -> 8
* __KALASHNIKOV__ renamed AK-47
* __CRAZY IVAN__ new weapon


## 0.3

### ninja rope:

* "nrAdjustVel" 1.5=>2.5

### COMMON WEAPONS:

* __BLACK BETTY__ changed bullet?
* __SPAS-12__ bulletSpeedInherit (0 -> 0.14), ammo (6 -> 5), delay (75 -> 80)
* __MAGNUM SRIFLE__ bulletSpeed (4 -> 3), bullet blowAway (0.6 -> 0.3), no more trail object
* __MACHINEPISTOL__ renamed => STEYR TMP, bullet detectDistance (1 -> 2)
* __MINI GUN__ renamed => XM214 MINIGUN 
* __NIGHT HAWK__ bullet (detectDistance (1 -> 2), blowAway (0 -> 0.98))
* __BARRETT M82A1__ new weapon
* __GLOCK BURSTFIRE__ renamed => GLOCK18 BURSTFIRE, delay (20 -> 23)
* __BENELLI M1014__ new weapon
* __FORCE FIELD__ timeToExplo (2700 -> 2100)
* __AUTOCANNON__ ammo (9 -> 8)
* __COLT-CARBINE__ renamed => COLT M4A1, bullet detectDistance (1 -> 2) 
* __UMP__ renamed => H&K UMP
* __VIRUS__  bulletSpeed (3.8 -> 2.3), loadingTime (999 -> 800), hmm.. and more damage
* __USP TACTICAL__ renamed => H&K USP TACTICAL
* __USAS-12__ bulletSpeedInherit (0 -> 0.2), loadingTime (325 -> 360), bullet hitDamage (8.4 -> 8)
* __PARA M249__ renamed => M249 SAW, bullet (detectDistance (1 -> 2), hitDamage (44 -> 45))
* __CLUSTERBOMB__ removed
* __RPG-7__ bulletSpeed (1 -> 1.5), bulletSpeedInherit (0.67 -> 0.15), recoil (0.13 -> 2), bullet (detectDistance (3 -> 2), distrib (0.0305.. -> 0.15))
* __DUAL 96G BERETTAS__ new weapon
* __P-90__ renamed => FN P90, bullet detectDistance (1 -> 2) 
* __FAUST__ new weapon
* __SIG SG-550 SN__ bulletSpeedInherit (0.01 -> 0), ammo (8 -> 11), delay (29 -> 23) loadingTime (670 -> 650), bullet distrib (0.28 -> 0), hitDamage 100 -> 97
* __BARRETT XM109__ new weapon
* __SHOCKGRENADE__ ammo (4 -> 2)


### SPECIAL WEAPONS:

* __MINI GUN__ renamed => XM214 MINIGUN
* __RPG-7__ bulletType (60 -> 29), bulletSpeed (1 -> 1.5), bulletSpeedInherit (0.67 -> 0.15), delay (20 -> 12), loadingTime (700 -> 500)
* __KALASHNIKOV__ removed
* __BOOBY TRAP__ ammo (2 -> 1)
* __GLOCK BURSTFIRE__ renamed => GLOCK18 BURSTFIRE
* __GAUSSGUN__ delay (85 -> 22), loadingTime (200 -> 180), bullet timeToExplo (121 -> 110)
* __HEALING ZONE__ timeToExplo (0 -> 7200)


## 0.29b

### COMMON WEAPONS:

* __CLUSTER MINE__ less loadingTime (1500 -> 800), less ammo (2 -> 1)
* __CANNONBALL__ higher bulletSpeed (2.4 -> 2.9), less loadingTime (840 -> 550), less ammo (4 -> 3), bullet gravity (0.035 -> 0.044), changed explosion
* __AIR MINE__ less ammo (2 -> 1)
* __STEALTH MINE__ new AIR MINE type weapon that hides itself after being thrown
* __UMP__ delay (8 -> 9)
* __FORCE FIELD__ distribution removed (0.00006103515625 -> 0), less ammo (2 -> 1)
* __VIRUS__ higher bulletSpeed (3.7 -> 3.8), less ammo (2 -> 1), higher bullet detectDistance (4 -> 5), lower gravity (0.0847625732421875 -> 0.0447625732421875), and higher timeToExplo (20 -> 25), slightly faster particles (0.13 -> 0.23), with more speed variation (0.1 -> 0.28) and less timeToExplo (1000 -> 470) / timeToExploV (300 -> 200)
* __MORTAR__ less ammo (3 -> 2)

### SPECIAL WEAPONS:

* __AIR MINE__ less ammo (3 -> 1)
* __FORCE FIELD__ removed completely
* __HEALING ZONE__ new name for the special STIMPACK
* __M I R V E__ higher damage on particles (8 -> 17)

## 0.29a

### COMMON WEAPONS:

* __CANNONBALL__ new weapon
* __NIGHT HAWK__ removed blowAway and gravity on the wobject, changed sobject on explosion
* __MAC-10__ higher hitDamage on wobject (4 -> 4.4)
* __USAS-12__ higher hitDamage (8 -> 9) and detectDistance (1 -> 2) on wobject, added objTrailType 4

## 0.29

### COMMON WEAPONS:

* __H&K G3/SG-1__ lower delay (27 -> 26)
* __MAC-10__ more ammo (65 -> 70), higher loadingTime (530 -> 630), lower bullet hitDamage (8 -> 4)
* __USP TACTICAL__ higher bulletSpeedInherit (0.3203030303030304 -> 0.3503030303030304)
* __USAS-12__ new weapon
* __DESERT EAGLE__ higher delay (23 -> 24)
* __APSHOCKCANNON__ more ammo (3 -> 4)

***

## 0.28d

### COMMON WEAPONS:

* __H&K G3/SG-1__ higher delay (24 -> 27), no more bullet gravity
* __MAC-10__ more ammo (40 -> 65), higher loadingTime (530 -> 630), lower bullet hitDamage (8 -> 4)
* __FLAMETHROWER__ more ammo (125 -> 135)
* __USP TACTICAL__ higher delay (22 -> 25), higher loadingTime (490 -> 690), bulletSpeedInherit  (0.30303030303030304 -> 0.3203030303030304), higher bullet gravity & distribution
* __DESERT EAGLE__ higher delay (18 -> 23)
* __SIG SG-550 SN__ higher delay (22 -> 29)
* __MINI GUN__ higher hitDamage for each bullet (14 -> 15)

### SPECIAL WEAPONS:

* __FLAMETHROWER__ more ammo (80 -> 120)
* __VIRUS__ higher particle speed (1.13 -> 1.23), higher particle hitDamage (22 -> 25)


***

## 0.28c

### COMMON WEAPONS:

* __MAC-10__ new weapon
* __NAILGUN__ slightly less delay (9 -> 8), bullet blow away (0.26 -> 0.23)
* __SIG SG-550 SN__ slightly higher delay (21 -> 22)
* __H&K G3/SG-1__ detect distance (2 -> 1)

### SPECIAL WEAPONS:

* __FLAKCANNON__ ammo (19 -> 37), delay (8 -> 5), loading time (240 -> 340), timeToExplo (0 -> 25 / variation 0 -> 6)

### SPRITES UPDATE
### COLORANIM CHANGED
* the 129->131 range has been removed from colorAnim

***

## 0.28b

### COMMON WEAPONS:

* __DESERT EAGLE__ slighlty higher delay (17 -> 18), higher loading time (490 -> 790)
* __STIMPACK__ negative recoil (0 -> -1.5), less delay (50 -> 0)
* __NAILGUN__ bullet blow away (0.22 -> 0.26)

### SPECIAL WEAPONS:

* __STIMPACK__ timeToExplo (800 -> 0)
* __BARRACUDA__ REMOVED Trap version
* __M I R V E__ on nObject => less blowAway (0.2 -> 0.1), more hitDamage (3 -> 8) 

### SPRITES UPDATE

***

## 0.28a

### SPECIAL WEAPONS:

* __STIMPACK__ timeToExplo (800 -> 0)
* __BARRACUDA__ REMOVED Trap version
* __M I R V E__ on nObject => less blowAway (0.2 -> 0.1), more hitDamage (3 -> 8) 

### SPRITES UPDATE

***

## 0.28

### COMMON WEAPONS:

* __SIG SG-550 SN__ more ammo (7 -> 8)

### SPECIAL WEAPONS:

* __HELLFIREROCKETS__ less ammo (10 -> 6)
* __STIMPACK__ new weapon

### SPRITES UPDATE

***

## 0.27e

### SPECIAL WEAPONS:

* __BARRACUDA__ new weapon
* __BARRACUDA__ new trap weapon (explodes when picked up)

***

## 0.27d

### COMMON WEAPONS:

* __SPAS-12__ less ammo (8->6), higher delay (66->75)
* __H&K G3/SG-1__ less ammo (9->8)
* __AUTOCANNON__ less ammo (10->9), slightly higher delay (25->26), more loading time (570->770)
* __STINGER__ more loading time (580->780), less detectDistance (3->1, nObject 3->1), less speed and blowAway on nObject
* __APSHOCKCANNON__ more loading time (550->600), less detectDistance (2->1)
* __AUTOCANNON__ less detectDistance (2->1), slightly higher distribution (0.4425048828125->0.4445048828125)
* __PARA M249__ more hitDamage (41->44)
* __MIRVE__ less detectDistance (5->3)

### SPECIAL WEAPONS:

* __STINGER__ more parts (bullet fired at once) (2->3), less detectDistance (3->1)
* __NAILGUN__ less ammo (100->50), more loading time (700->800), less hitDamage (30->20)
* __VIRUS__ more splinters (99->120)
* __BADGER__ timeToExploV (8250->9200)
* __M I R V E__ new weapon
* __HELLFIREROCKETS__ new weapon

### SPRITES UPDATE

***

## 0.27c

### COMMON WEAPONS:

* __HELLFIREROCKETS__ less ammo (30->25)
* __APSHOCKCANNON__ less ammo (4->3), more loading time (500->550), lower detectDistance (3->2)
* __NAILGUN__ less ammo (150->50), more loading time (400->700)
* __SHOCKGRENADE__ more loading time (500->650), lower detectDistance (4->2)
* __FORCE FIELD__ timeToExplo (3600->2700)
* __PARA M249__ more hitDamage (39->41)
* __CLUSTER MINE__ less splinters on second phase (39->33)

### SPECIAL WEAPONS:

* __NAILGUN__ less ammo (200->100), more loading time (400->700)
* __BOOBY TRAP__ more ammo (1->2), faster bullet (1.3->1.8, inherit 0.1->0), changed frame, shotType (0->1), splinter amount (3->255) and type


***

## 0.27b

### COMMON WEAPONS:

* __CLUSTER MINE__ less ammo (3->2), higher loadingTime (720->1500), bullet speed inherit (0.27932->0.1), detect distance (3->0), wormCollide/explode=> false, time to expload  lowered (1600/variation 1000->45), splinter (39->1) (well widely changed ^^)
* __AIR MINE__ slightly higher bullet speed (5.4->5.5), lower timeToExplo (65->15)
* __DESERT EAGLE__ slightly higher delay (16->17)
* __STINGER__ less ammo (3->2), more loading time (480->580)
* __FORCE FIELD__ timeToExplo (1800->3600)
* __NAILGUN__ bloAway (0->0.22), hitDamage (25->30)
* __SHOCKGRENADE__ detectDistance (1->3)

### SPECIAL WEAPONS:

* __RPG-7__ new weapon
* __NAILGUN__ more ammo (150->200)
* __GLOCK BURSTFIRE__ more ammo (7->12)
* __BADGER__ less splinter (510->400), more variation on timeToExploV (360->8000)
* __FORCE FIELD__ timeToExplo (7200->0)
* __BOOBY TRAP__ bounce (0.1->0.4), splinters (2->3)

***

## 0.27a

### COMMON WEAPONS:

* __SHOCKGRENADE__ more ammo (2->4), less delay (120->95)
* __EXCIMER LASER__ more damage (18->20)

### SPECIAL WEAPONS:

* __VIRUS__ less delay (160->100)
* __GRENADE__ less delay (150->60)
* __AIR MINE__ more splinters (44->255)
* __BADGER__ less splinter (510->400), more variation on timeToExploV (360->8000)

***

## 0.27

### COMMON WEAPONS:

* __H&K G3/SG-1__ more ammo (7->9), slightly less delay (25->24)
* __HELLFIREROCKETS__ more ammo (21->30), higher distribution of bullets, less blowAway (0.35->0.32), slightly higher bullet hitDamage (11->12)
* __NAILGUN__ more ammo (63->150), slightly less delay (10->9), parts 2->2.2 (?)
* __SHOCKGRENADE__ new weapon
* __APSHOCKCANNON__ stronger gravity

### SPECIAL WEAPONS:

* __MINI GUN__ more ammo (180->300)
* __NAILGUN__ more ammo (100->150)
* __BADGER__ new weapon

### SPRITES UPDATE (BADGER)

***

## 0.26c

### COMMON WEAPONS:

* __AUTOCANNON__ less ammo (20->10), higher delay (20->25), more loadingTime (400->570), higher gravity, more splinters (3->10), slight delay on object trail (0->1)
* __NIGHT HAWK__ less loadingTime (340->200)
* __MINI GUN__ slightly higher hitDamage (13->14)
* __EXCIMER LASER__ slightly higher hitDamage (17->18)
* __DESERT EAGLE__ slight delay on object trail (0->1)
* __NAILGUN__ slightly higher hitDamage (22->23)


### SPECIAL WEAPONS:

* __AIR MINE__ faster bullet (2.6->3.6), less ammo (6->3), much higher loadingTime (800->999999), wormExplode/collide enabled, timetoExplo =>0
* __VIRUS__ more ammo (1->4), higher delay (100->160), much lower loadingTime(999999->680), higher hitDamage (14->22), explodes on ground


***

## 0.26b

### COMMON WEAPONS:

* __AIR MINE__ less ammo (5->2)
* __CLUSTERBOMB__ less ammo (3->2)

### SPECIAL WEAPONS:

* __VIRUS__ new weapon
* __GLOCK BURSTFIRE__ new weapon
* __GRENADE__ new weapon

***

## 0.26a

### COMMON WEAPONS:

* __FLAKCANNON__ explGround true
* __FORCE FIELD__ slightly higher damage (13->14)

### SPECIAL WEAPONS:

* __FORCE FIELD__ new weapon
* __FLAKCANNON__ new weapon
* __MINI GUN__ more ammo (140->180)
* __NAILGUN__ less damage (22->25)
* __GAUSSGUN__ higher detect distance (6->8)

***

## 0.26

### COMMON WEAPONS:

* __FLAKCANNON__ new weapon
* __STEYR SCOUT__ slightly faster bullets, less loading, more ammo, higher damage
* __FORCE FIELD__ stays longer (1800 frames), higher damage
* __EXCIMER LASER__ higher damage

### SPECIAL WEAPONS:

* __GAUSSGUN__ new weapon

***

## 0.25c

### COMMON WEAPONS:

* __STEYR SCOUT__ upgrade, changed from direct bullet hit to partrail object damage

### SPECIAL WEAPONS:

* __KALASHNIKOV__ 

***

## 0.25b

* __RPG__ bulletSpeedInherit 1 to 0.67 , slightly higher damage from explosion splinters (9 to 10) (splinters also stay longer by 3 frames), more damage from explosion (45 to 47 detectRange, 22 to 25 damage)
* __STIMPACK__ ammo buffed from 1 to 2, delay brought down from 199 to 50, gives more health
* __FORCE FIELD__ stays longer (1580 frames), slightly higher damage (10 to 11)
* __EXCIMER LASER__ slightly lower damage (21 to 15)
* __NAILGUN__ slightly higher damage (19 to 22) (both nailguns)
* __SIG__ higher distribution, less distance for collision detection

***

## 0.25a

### COMMON WEAPONS:

* __apshock__ Finally the 'shock' in the weapon's name has a meaning. The particles created by the explosion can't be ignored as it was in the previous version of this weapon. 
* __rpg__ This weapon was rather incomplete. The explosion was powerful but the graphics wasn't showing the power at all. Also +1 to ammo
* __minigun__ Such a fun weapon to play, but unfortunately it's banned. That's why a new one is added as a common weapon (the old one can be found in crates as always). Not as oppressive as its predecessor with lowered fire ratio, less ammo, faster bullets, slightly decreased damage and spread.
* __nailgun__ Similar to minigun, a weaker nailgun with 2 nails instead of 4 is added as a common weapon. Damage 15-19, less ammo.
* __force field__ Fire and forget. Sometimes an enemy will be killed, sometimes it kills you. Force field is not used with intention and it works mostly because it's barely visible so I changed this weapon quite significantly. 
* __sig/h&k__ The original idea was to make them faster than black betty/magnum but with less precision. They turned out to be precise enough to become the strongest weapons in csliero so several changes need to be made. SIG: Increased the distribution but the movement doesn't have much effect on the bullets. H&K: Very precise when a player stands still, however any movement hurts your aim a lot 
* __hellfirerockets__ Ammo 19->21, dmg 7->11, reload 880->500,
* __BLACK BETTY__ less recoil
* __STEYR SCOUT__ throws only one bullet at once, more ammo (10 to 15) less delay (85 to 75) higher detect distance, blowAway and distribution, but less damage on direct hit (100 to 29)

### SPECIAL WEAPONS:

* __stinger__ Two missile stinger can be found in weaponcrates
* __darude sandstorm__ Stronger version of darude can be found in weaponcrates
* __air mine__ A special version of air mine can be found in weaponcrates
* __nailgun__ Dmg 15->19
* __plasma rockets__ Safer to use, 2 rockets at once, ammo +1
* __minigun__ Reload 1300-900
  
### NEW WEAPON:

* __stimpack__ Health regeneration(+66hp), one ammo, no reload

### OTHER:

* __Plasma rockets(FLAMER)__ renamed to __PLASMA ROCKETS__
* __FLAMER__ has been renamed to __FLAMETHROWER__. Fits better IMO

***

## 0.24

* rebalancing some weapons
* added a third bonus FLAMER with a different look

***

## 0.23

* rebalancing some weapons
* added second (super) FLAMER (intended to be bonus-only)

***

## 0.22

* changes in physics (constans part)
* lots of sh*t weapons removed
* other weapons rebalanced
