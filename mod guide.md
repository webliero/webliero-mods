# Webliero Mod Guide ⚙️

created by [Scharnvirk-Jerac](https://github.com/Scharnvirk), expanded by [roo](https://github.com/KangaRoo1372) & [vitu](https://github.com/Victorcorcos)

This mod guide will help you understand how to modify [WebLiero](https://www.webliero.com/) mods and even make your own ones. Enjoy!

Note: this file documents the purpose behind each parameter of the json/json5 file (which contains "logic" part of the mod) and also gives some information about wlsprt file (which contains gfx part of the mod).

To create & modify mods for WebLiero, you can use [wledit](https://www.vgm-quiz.com/dev/webliero/wledit/), which is a special tool designed for it, with lots of cool options and stuff (and if you have also [WebLiero Extended hack](https://www.vgm-quiz.com/dev/webliero/extended) installed, you will be able to "live edit" your mod in the room using wledit and test your changes real-time, without the need to reupload the mod files in the room after every change; to use this feature, you need to open wledit in the room with a special option available in the admin menu panel). I strongly recommend it!

For more useful information about WebLiero and Liero game logic, you can visit [wgetch's liero pages](https://liero.phazon.xyz/) and [Liero Hacking Project site](https://liero.nl/lierohack/).

# Summary 🔍

1. [Few general assumptions](#few-general-assumptions-%EF%B8%8F)
2. [Soundpack](#soundpack-)
3. [Constants](#constants-%EF%B8%8F)
4. [Weapons](#weapons-)
5. [wObjects](#wobjects-)
6. [nObjects](#nobjects-)
7. [sObjects](#sobjects-)
8. [Textures](#textures-)
9. [others](#others-)
10. [Extended](#extended-)
11. [WLSPRT](#wlsprt-%EF%B8%8F)
12. [Credits](#credits-)

## Few general assumptions ⚠️
- Game does 60 physics ticks, or "frames", per second
- All times, durations etc. are shown in frames
- unlike original Liero, in WebLiero there are technically no max/min limits of values for int properties, but please note that using absurdly high values may cause lags (and in some cases even desynchronisation of players - see more info about "timeToExplo" parameter in wObjects and nObjects)
- minimum amount of weapons in one mod is 1 (game will freeze if you try to load a mod with 0 weapons; however, in [WebLiero Extended](https://www.vgm-quiz.com/dev/webliero/extended) such non-weapon mods could be used to merge them - using special room scripts - with other "normal" mods, especially to make "elements", i.e. objects spawnable by room script, e.g. lava, water, etc.); please also note that worms always have 5 active weapons, so if you make a mod with less than 5 weapons, you will get weapons "duplicated" anyway during the gameplay
- maximum amount of weapons in one mod is 255 (0-254; however, weapon ID 250 is used as the RANDOM weapon id, so if you make a mod with 251-255 weapons, then weapons would not be randomized; so, if you set e.g. CHAINGUN for weapon ID 250 and set all your weapons to random, then you will get 5x CHAINGUN every time); game will freeze if you try to load a mod with more than 255 weapons
- maximum amount of wObject, nObjects, sObjects and textures is 255 (for every array separatedly); game will freeze if you try to load a mod with more than 255 wObjects, 255 nObjects etc.
- unlike original Liero, in WebLiero all int properties can have negative values set, as well as decimal numbers (not only integers or natural numbers)
- every sObject MUST have startFrame>=0 (if you set -1 for it, weird things gonna happen with sObject animation  and the game can even freeze)
- [DON'T set splinterType -1 and splinterAmount > 0 at once (this will freeze the game)](https://gitlab.com/webliero/webliero-issues/-/issues/29)
- don't make weapons which generate big amount of powerful sObjects (with high detectRange and damage), it makes game laggy
- worm always bleeds on collision with sObject with damage ≠ 0 and detectRange ≠ 0 (unlike wObject and nObject which have bloodOnHit parameter that you can use to turn off blood even when wObject or nObject has got damage ≠ 0)
- amount of blood created on worm collision with sObject is not affected by damage, but detectRange parameter
- [startFrame: 0 doesn't work for nObjects (it works the same as startFrame: -1)](https://gitlab.com/webliero/webliero-issues/-/issues/38)
- worm will not receive damage from nObject with hitDamage ≠ 0 (and also the pushback force determined in the blowAway parameter of nObject will not be applied to the worm) if you set its TimeToExplo parameter to 1 (to make it work, TimeToExplo value must be set to at least 2 or equally 0); in such case, the sprite of such nObject will not be displayed either
- there are some specific indices of nObjects and sObjects (and textures) that need to have items filled and are used on the internals of the game, i.e. mandatory objects (dirtEffects). If these objects (dirtEffects) are removed from the objects (textures) array, the game becomes buggy, can freeze and can generate console errors in the browser, so be aware of it when you modify them (see points 5-8 and 19 on the list below)
- there are some specific sprites that need to have items filled and are used on the internals of the game, i.e. mandatory sprites. If these sprites are missing in the WLSPRT file, the game becomes buggy, can freeze and can generate console errors in the browser, so be aware of it when you modify them (see points 10-13, 15 and 18 on the list below)
- there are some hardcoded things which you can't change in json file, i.e.:
1. time after bonuses explode: 75 seconds
2. sounds used for some events (throw, death, hurt, alive, bump, reloaded, movedown) cannot be changed for different sound id (except for "reloaded" - you can set custom reload sound for every weapon; this feature is available only in WebLiero Extended though, however currently it does not work yet); [read more about that in the "soundpack" section](#soundpack-)
3. you cannot disable rope completely ([such feature was implemented in WebLiero Extended room though](#extended-))
4. worm starts bleeding when its HP level is less than 25
5. mandatory nObjects: nObject6 (blood particle), nObject7 (shell), nObject0 (worm part - gibs), nObject2 (particle created when sObject explodes near dirt on the map)
6. when worm is killed, there will be generated always 8 worm parts (nObject0)
7. mandatory sObjects: sObject0 (used for explosion of bonuses), sObject7 (used when bonuses appear on the map)
8. worm always uses dirt mask (dirtEffect) 7 when digging dirt
9. wObject34: if you set "true" for "affectByExplosions" parameter, this object won't be pushed away by explosion's force on collision with sObject (like other wObjects), but it will be removed from the map. That's because wObject34 was originally (in Liero 1.33) a booby trap and intended to behave like bonuses (bonuses explode on sObject explosion)
10. sprite used for flag object in flag-type game modes (you can modify the sprite itself, but cannot change the sprite id: 214)
11. sprites used for health & weapon bonuses (you can modify the sprite itself, but cannot change the sprite id: 210 for weapon pack and 211 for health pack)
12. flag properties in flag-type game modes (gravity, bounce etc.)
13. sprites used for firecone (you can modify sprites, but cannot change sprites indices: 9-15)
14. colours used for laserSight and its opacity (you can only change colours by modifying palette, but you cannot change its opacity or the colour indices: 83-84)
15. sprite used for hook in rope (you can modify the sprite itself, but cannot change the sprite id: 84)
16. colours used for blood trail particles when they stain on rock/dirt/ground (you can change the colours by modifying palette, but you cannot change colour indices: 77-87); you cannot also change appearance of blood trail particles, i.e. they are always coloured pixels (you cannot set custom sprite as a start frame for blood trail particles)
17. [crosshair position in x/y offset](https://gitlab.com/webliero/webliero-issues/-/issues/14)
18. worm sprites indices and their animation order (you can modify sprites, but cannot change sprite indices: 16-36)
19. dirtEffect used when worm is spawned on the map (you can modify the dirtEffect itself in the section "textures", but cannot change its index for this event: 0)
20. worm speed division when it "bounces" off rock / dirt / edge od the map ([see more explanations in minBounce parameters in "constants" section](#constants-%EF%B8%8F))

- object relations (the connection between objects):

1. In `weapons` you have `bulletType`: refers to the `wObject` id. Created on gunshot.
2. In `wObject` and `nObject` you have `createOnExp`: refers to `sObject` id. Created on explosion.
3. In `wObject` you have `objTrailType`: refers to `sObject` id. Created on trail.
4. In `wObject` and and `nObject` you have `splinterType`: refers to `nObject` id. Created on explosion. `splinterAmount` refers to the amount of such objects created on explosion.
5. In `wObject` you have `partTrailObj`: refers to `nObject` id. Created on trail.
6. In `nObject` u have `leaveObj`: refers to `sObject` id. Created on trail.

[[scroll to the top](#summary-)]

## Soundpack 🔉

Which `soundpack` is used.

Those are stored on the server side and cannot be changed or modified yet (unless you have [WebLiero Extended hack installed](https://www.vgm-quiz.com/dev/webliero/extended)), you can only switch between few predetermined ones. Currently there are 6 predefined soundpacks:

1. sorliero.zip (dedicated for [Magic Playground](https://gitlab.com/webliero/webliero-mods/-/tree/master/Jerac/MagicPlayground))
2. promode.zip (dedicated for [Promode Final](https://gitlab.com/webliero/webliero-mods/-/tree/master/pro_mode) & [Promode ReRevisited](https://gitlab.com/webliero/webliero-mods/-/tree/master/Jerac/ReRevisited))
3. csliero.zip (dedicated for [csliero 2.2b](https://gitlab.com/webliero/webliero-mods/-/tree/master/csliero%202.2b))
4. blackwindv1.zip (dedicated for [Blackwind Liero TC](https://gitlab.com/webliero/webliero-mods/-/tree/master/%5Bold%20mods%20converted%5D/BLACKWIND%20-%20Blackwind%20TC%20v1))
5. blackwindv2.zip (dedicated for [Windstorm Liero TC](https://gitlab.com/webliero/webliero-mods/-/tree/master/%5Bold%20mods%20converted%5D/BLACKWIND%20-%20WINDSTORM%20Liero%20TC%20v2))
6. default soundpack for classic [Liero 1.33](https://gitlab.com/webliero/webliero-mods/-/tree/master/classic) (will be applied if you don't use 'soundpack' function)

Every soundpack is actually a custom .snd file format (packed in .zip), which contains 30 sounds, counted from 0 to 29 (however, in wledit you can make a soundpack with more than 30 sounds; such soundpack would work properly in WebLiero). Every sound must be PCM mono 8bits 22050 Hz unsigned .WAV file. Every sound has got its own name which can be max eight characters long.

Please note that some sounds are used in the game engine for some events and this is hardcoded which means that you cannot change sound indices for such events:

- 5 (THROW - ninja rope throw sound)
- 14 (BUMP - played when worm bumps)
- 16-18 (DEATH2, DEATH3, HURT1 - played when worm dies; it should be 15-17 (DEATH1, DEATH2, DEATH3) instead, [however there is a bug in the game code](https://gitlab.com/webliero/webliero-issues/-/issues/39))
- 18-20 (HURT1, HURT2, HURT3 - played when worm receives damage and when it bleeds)
- 21 (ALIVE - played on worm respawn)
- 24 (RELOADED - played on weapon reload and when worm picks weapon bonuses on the map; however, in WebLiero Extended you can set custom reload sound for every weapon; this feature currently does not work yet though)
- 26 (MOVEDOWN - used as a counter for the last 10 seconds of the game, played per every second)

For more information about .snd file format and sounds, see [this page on Liero Hacking Project site](https://liero.nl/lierohack/docformats/liero-snd.html).

```js
soundpack: 'promode.zip',
```
[[scroll to the top](#summary-)]

## Constants 🕹️

The `constants` define general behavior and basic parameters of the mod, like physics or gravity of worm, bonuses and ninja rope.

```js
constants: {
    nrInitialLength: 250, // initial lenght of ninja rope when shot (note: affected by ninjaropeGravity and nrThrowVel)
    nrAttachLength: 28.125,    // lenght of ninja rope when attached to rock/dirt obstacle, edge of the map or other worm
    minBounceUp: -0.8125,  // minimum speed at which worm "bounces" (worm velocity / 3) when it hits something while moving up; if hFallDamage is set "true", it also indicates worm minimum speed to get hurt in such event
    minBounceDown: 0.8125, //  minimum speed at which worm "bounces" (worm velocity / 3) when it hits something while moving down; if hFallDamage is set "true", it also indicates worm minimum speed to get hurt in such event
    minBounceHoriz: 0.8125,  //  minimum speed at which worm "bounces" (worm velocity / 3) when it hits something while moving horizontally (left/right); if hFallDamage is set "true", it also indicates worm minimum speed to get hurt in such event
    wormGravity: 0.02288818359375,   // worm gravity
    wormAcc: 0.0762939453125,  // worm accelerate
    wormMaxVel: 0.969482421875,  // worm maximum velocity
    jumpForce: 0.85546875,   // worm jump force
    maxAimVel: 0.052430983960935, // max aim velocity
    aimAcc: 0.0029960562263391427, // aim acceleration
    ninjaropeGravity: 0.0152587890625,   // ninja rope gravity
    nrMinLength: 10.625, // ninja rope minimum lenght (when shortening it)
    nrMaxLength: 250,  // ninja rope maximum length (when lenghtening it)
    bonusGravity: 0.02288818359375, // bonus gravity; bonuses are health and ammo/weapon packs
    wormFricMult: 0.89, // worm friction multiplier
    aimFric: 0.83, // aim friction
    nrThrowVel: 8, // ninja rope throw velocity
    nrForce: 0.08333333333333333, // ninja rope pull strength
    bonusBounce: 0.4,  // bonus speed multiply on bounce. After bouncing, bonus will get this percentage of its original speed
    bonusFlickerTime: 220, // amount of time in game ticks before the bonus pack explodes; at this point it starts flickering
    aimMaxAngle: 1.5707963267948966,  // aim maximum angle (when using up arrow; 1/2π is 90° angle)
    aimMinAngle: -1.521708941582556,  // aim minimum angle (when using down arrow; -1/2π is 90° angle)
    nrAdjustVel: 1.5, // ninja rope adjust velocity; used when you shorten and lenghten the rope
    nrColourBegin: 24,  // ninja rope colour begin (id taken from palette); if nrColourBegin > nrColourEnd, then the rope will use all colours from the palette between nrColourBegin ID+1 and nrColourEnd ID; if nrColourBegin = nrColourEnd, then the rope will use colour ID 0 only
    nrColourEnd: 27, // ninja rope colour end (id taken from palette); if nrColourEnd > nrColourBegin, then the rope will use all colours from the palette between nrColourBegin ID and nrColourEnd ID-1; if nrColourEnd = nrColourBegin, then the rope will use colour ID 0 only
    bonusExplodeRisk: 100, // risk for weapon bonuses to explode on worm touch (1/X); currently doesn't work in WebLiero (see WebLiero issues #40)
    bonusHealthVar: 20, // (maximum) variance of health given by bonus
    bonusMinHealth: 10, // minimum bonus health. So in this setting healing range is 10-30
    firstBloodColour: 80,  // colour of blood as a trail (id taken from palette)
    numBloodColours: 2,  // amount of colours to use for blood as a trail
    bObjGravity: 0.0152587890625, // gravity of blood as a trail
    splinterLarpaVelDiv: 3,  // division of velocity of all larpa-type nObjects (partTrailType: 1): wObject speed / X
    splinterCracklerVelDiv: 3,  // division of velocity of all crackler-type nObjects (partTrailType: 0): wObject speed / X
    fallDamageHoriz: 0,  // damage received by worm when it hits something while moving horizontally (left/right) with minimum speed indicated in minBounceHoriz parameter
    fallDamageDown: 0,  // damage received by worm when it hits something while moving down with minimum speed indicated in minBounceDown parameter
    fallDamageUp: 0,  // damage received by worm when it hits something while moving up with minimum speed indicated in minBounceUp parameter
    hFallDamage: false, // activates/deactivates "fallDamage" properties. Note: when set "true", then "bump" sound from the soundpack (sound id 14) is not being played when worm hits something.
    hBonusReloadOnly: true, // if true, weapon bonuses will only do reloads, if false - will give you a new weapon. Currently doesn't work in WebLiero (even if set "true", it gives new weapon instead of doing reload only - see WebLiero issues #36)
  }, // end of constants section
```
[[scroll to the top](#summary-)]

## Weapons 🔫

A `Weapons` section contains general description on how every **weapon** works.

Think of it as if it was describing properties of the pistol itself, how does it affect the worm (recoil, noise, etc.), but not the bullet.

Note: if you make a mod with 5 weapons and play it with random weapons, [you will still have weapons duplicated, even though you set 0 for the max duplicate weapons in room options](https://gitlab.com/webliero/webliero-issues/-/issues/21).

```js
  weapons: [
    {
      /*
        Defines how many objects (particles) the weapon shoots. 1 for pistols, bazookas etc., 20 will be a shotgun-type weapon.
      */
      parts: 20,

      /*
        Which bullet type (wObject) to use.
        Bullets (wObjects) are stored in an ordered array in "wObjects" section (counting is started from 0).
        So, if you have e.g. a chaingun bullet stored in third position, you need to set "bulletType: 2" here.
      */
      bulletType: 0,

      /*
        Initial speed of the bullet. 6 is about the max playable value for usual weapons.
        Note: if you set too high value for it, the bullet might pass through worms and even through thinner walls.
        Note: To make very fast gauss-like weapons, use "repeat" property.
        Note: if you set negative value for it, the bullet will go in the opposite direction the worm is aiming.
      */
      bulletSpeed: 5,

      /*
        Percentage of worm's speed applied to the bullet.
        Note: this is affected by "repeat"!
      */
      bulletSpeedInherit: 0.2298850574712644,

      /*
        This parameter doesn't do anything - only the wObject distribution property matters for bullets. Probably a bug.
      */
      distribution: 0.25,

      /*
        Pushback force with which the worm is thrown away when firing a weapon.
      */
      recoil: 0.55,

      /*
        Number of shots before the weapon needs to be reloaded.
      */
      ammo: 8,

      /*
        Delay time (in frames) between individual shots of the same weapon.
      */
      delay: 28,

      /*
        Reload time.
      */
      loadingTime: 400,

      /*
        Frequency of shells ejected. Set to 0,1,2,3 or 4. 0=never, 1=always, 2=sometimes, 3=rarely, 4=very rarely.
      */
      leaveShells: 1,

      /*
        Time between shot and shell ejected.
      */
      leaveShellDelay: 28,

      /*
        Duration of firecone sprite being displayed. It is taken from the sprite sheet, not animated and always uses same sprites (9-15, depending on crosshair position).
      */
      fireCone: 9,

      /*
        Whether a weapon has the flickering, red laser sight. It cannot be configured in any way, except for changing its colour (this requires palette changing though).
        Note: laser sight is not displayed on special rock (undefined) on the map (or after it), however the bullet itself passes through such type of material.
      */
      laserSight: false,

      /*
        Whether a weapon has a laser beam of a given color configured within its wObject's "colorBullets" parameter.
        Note: laser beam is drawn as long as you have some ammo and you press fire, even if "delay" parameter > 0.
        Note: in original Liero, this parameter was hardcoded, i.e. it was used only for wObject 28. In WebLiero, you can set it "true" for any wObject you like.
        Note: laser beam is not displayed on special rock (undefined) on the map (or after it), however the bullet itself passes through such type of material.
      */
      laserBeam: false,

      /*
        Sound played when the weapon is fired (taken from soundpack). Set -1 for none.
      */
      launchSound: 0,

      /*
        Play reload sound when the weapon is reloaded or not.
      */
      playReloadSound: true,

      /* 
        Name of the weapon.
        Note: unlike in original Liero, there are no limits regarding the lenght of the name of a weapon.
        Note: it is recommended to use only capital letters taken from latin script (English alphabet), without numbers or any special signs or other alphabets. This is because the game uses predefined sprites (taken from wlsprt file) for letters in two cases: to display weapon name above the worm (while changing weapons during game) and on weapon bonuses on the map.
        So, this means that if you name your weapon "Mn16", it will be displayed normally as "Mn16" e.g. on the weapon list in room options menu, but only as "M" in those two aformentioned cases.
        To learn more about the "sprite letters", see "textSpriteStartId" parameter in the "other" section.
      */
      name: 'AUTO SHOTGUN' 
    }
    // This repeats for all the other weapons defined in the game.
  ], // End of weapon descriptions' array
```
[[scroll to the top](#summary-)]

## wObjects 🚀

`wObjects` are **objects shot by weapons** (like gunshots).

Only weapons can produce them, and ordering in the array matters, counting from 0.

```js
  wObjects: [
    {
      // shotgun wid0 - Those are just my comments denoting the id and the type of the object. Those comments are really helpful though, because that way you can quickly find an object you are looking for, just by seaching for "nids", "wids" and "sids" ("wid0" stands for wObjectIDx, "nid0" stands for nObjectIDx etc.).

      /*
        Additional worm detect distance for the bullet. Affects the distance at which an object hits a worm. Add more for "bigger" bullets or things like proximity detonators.
        Note: this parameter also determines starting distance from player (the distance at which the object is created).
        Note: if detectDistance < 0, then the worm will not receive damage from the object and also blowAway/wormCollide parameters will not work.
      */
      detectDistance: 1,

      /*
        Force affecting the worm on hit.
        Note: this will also work if the object has "wormCollide" set to false; in such case, it will simply not disappear and push the worm continuously.
        Note: works only if the object is moving and detectDistance ⩾ 0!
      */
      blowAway: 0.01,

      /*
        Gravity of the object.
      */
      gravity: 0.01068115234375,

      /*
        Sound played on object explosion (taken from soundpack). Set -1 for none.
        Note: an object may be removed without explosion, in which case no sound will be played - see "wormExplode" and "explGround".
      */
      exploSound: -1,

      /*
        Works in two modes:
        a) for shotType = 3, this is additional speed added each frame. Use it for constant accelerating weapons.
        b) for shotType = 2 (directional player-controlled missile), this is an additional speed added to the missile when pressing up.
        It has no impact on other shotType (0, 1 and 4).
      */
      addSpeed: 0,

      /*
        Spread of the object. This works by adding a random direction vector of random length to current speed vector of the projectile.
        Note: if you set its value to > 1 or < -1, then more than 1 direction vector will be applied, so that the projectiles will have a variable initial velocity after firing.
      */
      distribution: 0.18310546875, 

      /*
        Speed multiplication each frame. Use it to have weapons which accelerate or decelerate non-linearly, like proxy mine from promode.
      */
      multSpeed: 1,

      /*
        Which special object (sObject) to use on explosion. Those are stored in ordered arrays and are defined near the end of mod json file. Set -1 for none.
      */
      createOnExp: 2,

      /*
        Which dirt mask to use on object explosion (see "textures" part of json file). Set -1 for none.
      */
      dirtEffect: -1,

      /*
        Whether the object should explode (produce a sObject and a sound indicated in exploSound) on worm collision.
        Note: works only if "wormCollide" is set "true" too!
      */
      wormExplode: false,

      /*
        Whether the object should explode (produce a sObject and a sound indicated in exploSound) on ground collision.
        Note: works only if "bounce" parameter equals 0.
      */
      explGround: true,

      /*
        Whether the object should collide with the worm and get removed.
      */
      wormCollide: true,

      /*
        Whether the object should collide with other objects. If yes, they will bounce off themselves but none of them will be destroyed. If set to false, they pass through each other.
        Note: this property doesn't work if wObject "blowAway" parameter equals 0!
        Note: this property works also if "detectDistance" parameter of the wObject is < 0.
      */
      collideWithObjects: false,

      /*
        Whether the object is affected by explosions' push force (on collision with sObject).
        Note: works only if the colliding sObject has got detectRange > 0 and damage ≠ 0 and blowAway ≠ 0!
      */
      affectByExplosions: false,

      /*
        Speed multiply on hitting rock/dirt obstacle or the edge of the map. After every bounce, the projectile will get this percentage of its original speed.
        Note: if you set it to -1, then the bullet will pass through rock / dirt (this "wallhack" feature works only for wObject, it doesn't work for nObject).
      */
      bounce: 0,

       /*
       Affects the angle at which the projectile will bounce.
      */
      bounceFriction: 0.8,

      /*
        Time to explode in frames (the duration time of the object before it gets removed from the map). When set to 0 there will be no explosion at all. 
        Any positive value will cause creation of a designated sObject indicated in createOnExp parameter (if not -1) and playing explosion sound indicated in exploSound (if not -1).
        Note: this parameter is affected by "repeat", which means that the higher the "repeat" value is set, the duration time before the object explodes will be proportionally shorter.
        Note: it is not recommended to set negative value for this property; it might cause desynch issues for players who use WebLiero Extended hack and play on non-extended rooms.
        Note: it is not recommended to set timeToExplo > 32767, because it might cause desynch issues for players who join the room right after the object with such property value is created on the map.
      */
      timeToExplo: 0,

      /*
        Maximum (negative) variation of time to explode in frames.
      */
      timeToExploV: 0,

      /*
        Damage inflicted on worm which was hit.
        Note: If the object has "wormCollide" property set to "false", this will be applied each frame the collision still occurs, leading to potentially huge damage values.
        Note: if you set negative value for it, you will have healing effect.
      */
      hitDamage: 1,

      /*
        Determines how many blood particles (nObject6) should be created on worm hit, divided by 2.
        So, if you set it to "10", then 5 blood particles will be created on worm hit (per each frame - which means that the amount of blood particles is affected by "wormCollide" parameter).
        Note: this property works also if wObject "hitDamage" parameter equals 0.
      */
      bloodOnHit: 3,

      /*
        First sprite of animation used for wObject.
        If -1, it will be a single pixel using a colour indicated in "colorBullets" parameter.
        Note: if you set -1 for this property and set "shotType": 2, then the bullet will be a single pixel but its colour will be changing depending on the object position (the direction the object is moving).
      */
      startFrame: -1,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame".
        Note: Animation begins on random frame, so it is suitable really only for objects which have animation cycle which looks good regardless of what frame it starts. Think things like spinning grenades, mines, pulsing items, etc.
        Note: works properly only for shotType 0, 1 and 4 (it's recommended to set this parameter to 0 for shotType 2 and 3).
        Note: the animation cycle is affected by "repeat" property, which means that the higher the "repeat" value is set, the faster the animation speed (the speed at which sprites change) will be (the delay before advancing to next frame will be lower).
      */
      numFrames: 0,

      /*
        Whether the animation should be looped.
        Note: loopAnim parameter is affected by bulletSpeed parameter, which means that if you set it "true", then the animation cycle of the wObject will work only if the object is moving (when wObject stops moving, the animation cycle stops too).
        Note: if loopAnim is set to "false" and numFrames > 0, then the object will be still animated; in that case, the animation cycle of the wObject will work also if the object is not moving (the animation cycle keeps going even when the bullet is not moving) - unless the bullet stops moving on collision with ground (dirt / rock / edge of the map; in that case, the animation cycle always stops).
        Note: if loopAnim is set to "true" and numFrames: 0 and shotType: 0, then the wObject will have randomly either the sprite indicated in startFrame or the next one in spritesheet (e.g. if you set startFrame 210, then the wObject will appear as sprite 210 or as 211; this is actually how booby trap shoots weapon packs or health packs by default).
        Note: works properly only for shotType 0, 1 and 4 (it's recommended to set this parameter to "false" for shotType 2 and 3).
      */
      loopAnim: false,

      /*
        Defines general type and behavior of wObject. 
        0 - a standard object being either a colored pixel or animated sprite.
        1 - a missile-type object which uses different frames in the animation depending on its direction (when the bullet is turned in different angles), but only if "numFrames" paramterer is set to 0; in that case,"startFrame" defines the start of directional sprite range in the spritesheet (full sprite range is 13 sprites including the one indicated in "startFrame" parameter). In this shotType, wObject is not affected by addSpeed parameter.
        2 - a player-controllable missile. It is animated like shotType: 1 (however, full sprite range is 16 sprites, including the one indicated in "startFrame" parameter, but only if "numFrames" paramterer is set to 0).
        3 - a missile-type object with "drunk" behavior when "distribution" is set to non-zero value. It is animated like shotType: 1 (full sprite range is 13 sprites including the one indicated in "startFrame" parameter, but only if "numFrames" paramterer is set to 0). In this shotType, wObject is affected by addSpeed parameter.
        4 - in original Liero this was a "Laser-type" weapon (the bullets were very fast; to achieve this effect, shotType: 4 was affected by "repeat" parameter and this was hardcoded, i.e. if shotType was set to 4, then wObject 28 had "repeat" set to 1000, and other wObjects to 8). However, since in WebLiero you can set "repeat" parameter manually for every shotType, then we can say that shotType: 4 in WebLiero behaves just like shotType: 0.
      */
      shotType: 0,

      /*
        How many times should the physics of the object be calculated per frame. This is how you make very fast projectiles which do not pass through walls or worm. Most notably, this affect percieved speed of the projectile.
        Note: unlike original Liero (in which "repeat" property was hardcoded), in WebLiero you can use any repeat value for any shotType, but please note that using absurdly high values may cause lags.
      */
      repeat: 1,

      /*
        Color of the object. Works only if you set a pixel (startFrame: -1) for a wObject.
        Note: this parameter also affects the colour of a laser beam created with "laserBeam" parameter.
      */
      colorBullets: 75,

      /*
        Amount of nObjects to create on explosion. The wObject must actually explode, for example if "wormExplode" is set to false and "wormCollide" is set to true, no nObjects will be created.
      */
      splinterAmount: 1,

      /*
        Color used on nObjects (produced as splinters) when they are a single pixel (startFrame -1 or 0). If splinterColour is set to 0 or 1, then splinters will have a colour indicated in nObject "colorBullets" property.
        Note: if splinterColour is set to 2 or more, nObject splinter will actually use two colours: the one indicated in this parameter, and also the previous one. So, in this case, splinters will use colour 13 and 12.
      */
      splinterColour: 13,
      
      /*
        Type of nObjects to create when the object explodes. This refers to index of the nObject in the array (counting started from 0), so if you change the order of nObjects, something else will be used.
      */
      splinterType: 2,

      /*
        Way in which the splinter (nObject) is scattered when the wObject explodes:
        0 = all directions (like in big nuke);
        1 = direction the wObject is moving (like in mini nuke).
      */
      splinterScatter: 0,

      /* 
        sObject (special Object) used as a trail. Set -1 for none.
        Note: wObject keeps creating sObjects on its trail even when it stops moving (and even when it's spawned or "trapped" inside rock or dirt on the map).
      */ 
      objTrailType: -1,

      /*
        Delay time (in frames) between creating trailing sObjects.
        Note: the delay is not referred to object's lifetime but game ticks, which means that it is measured in relation to the time elapsed since the game started, not since the object was created.
        Note: If "repeat" is set to greater than 1, sObjects will be created in "batches", creating intermittent lines instead of denser lines. This is probably a bug or unintended behavior.
      */
      objTrailDelay: 0,

      /*
        This is NOT a type of object used for trailing something.
        "partTrail" here refers to nObject trail, however partTrailType defines how will it be dropped:
        0 - crackler-type non-directional trail (objects are dropped in all directions); in this case, the speed of nObject is affected by "SplinterCracklerVelDiv" parameter (section "constants");
        1 - larpa-type directional trail (objects are dropped in the direction the wObject is moving); in this case, the speed of nObject is affected by "SplinterLarpaVelDiv" parameter (section "constants").
      */
      partTrailType: 0,

      /*
        This is the type of nObject trailed by the wObject. Set -1 for none.
        So, if you have e.g. smoke nObject stored in third position in the nObjects array and you want your wObject to create this smoke nObject as a trail, you need to set "2" here.
        Note: wObject keeps creating nObjects on its trail even when it stops moving (and even when it's spawned or "trapped" inside rock or dirt on the map).
      */
      partTrailObj: -1,

      /*
        Delay time (in frames) between creating trailed nObjects.
        Note: the delay is not referred to object's lifetime but game ticks, which means that it is measured in relation to the time elapsed since the game started, not since the object was created.
      */
      partTrailDelay: 0,

      /*
        Base speed used for missile-type weapons ("shotType" = 2). Use "addSpeed" property to define additional speed when pressing "up" button while flying.
      */
      speed: 435
    }
    // (...) more WIDs follow here
  ],  // End of wObjects descriptions' array
```
[[scroll to the top](#summary-)]

## nObjects 🎈

`nObjects` are **non-weapon** objects.

Those are things like worm gibs, dropped shells, blood, but also additional weapon particles like Chiquita bananas dropped by Chiquita Bomb.

Like other types of objects, they are indexed by their order in the array (counting started from 0).

```js
  nObjects: [
    {
      // worm part nid0 - description of the nObject. This is just a comment with denoting the id of every nObject, same like I did with wObjects.

      /*
        Additional worm detect distance for the bullet. This setting affects the distance at which an object hits a worm. Add more for "bigger" bullets or things like proximity detonators.
        Note: unlike in wObject, detectDistance here does not determine the starting distance from the player.
        Note: if detectDistance < 0, then the worm will not receive damage from the object and also blowAway parameter will not work.
      */
      detectDistance: 0,

      /*
        Gravity of the object.
      */
      gravity: 0.0152587890625,

      /*
        Initial speed of the particle.
        Note: this parameter works only for nObjects created as:
        a) splinters with "splinterScatter" parameter set to 0;
        b) trails with "partTrailType" parameter set to 0.
        Note: for splinters with "splinterScatter" parameter set to 1 and trails with "partTrailType" parameter set to 1, the speed of nObject is affected only by wObject actual speed!
      */
      speed: 1.2,

      /*
        (Negative) variation in initial speed of the particle.
      */
      speedV: 0.4,

      /*
        Spread of the particle. This works by adding a random direction vector of random length to current speed vector of the projectile.
        Note: if you set its value to > 1 or < -1, then more than 1 direction vector will be applied, so that the projectiles will have a variable initial velocity after firing.
      */
      distribution: 0.1220703125,

      /*
        Force affecting the hit worm. This will also work if the object has "wormDestroy" set to false; it will simply not disappear and push the worm continuously.
        Note: works only if the particle is moving and detectDistance ⩾ 0 and hitDamage ≠ 0!
      */
      blowAway: 0,

      /*
        Speed multiply on hitting rock/dirt obstacle or the edge of the map. After every bounce, the projectile will get this percentage of its original speed.
        Note: lack of "bounceFriction" property here!
        Note: unlike in wObject, setting negative value for bounce will not make nObject pass through walls.
      */
      bounce: 0.1,

      /*
        Damage inflicted on worm which was hit.
        Note: If the object has "wormDestroy" property set to "false", this will be applied each frame the collision still occurs, leading to potentially huge damage values.
        Note: if you set negative value for it, you will have healing effect.
      */
      hitDamage: 0,

      /*
        Whether the object should explode (produce a sObject) on worm collision. Works independently of "wormDestroy" parameter, which means that the object will explode also even if "wormDestroy" is set to "false".
        Note: this property doesn't work if nObject "hitDamage" parameter equals 0!
      */
      wormExplode: false,

      /*
        Whether the object should explode on ground collision.
        Note: works only when the object is no longer in motion.
      */
      explGround: true,

      /*
        Whether the object should collide with the worm and get removed.
        Note: this property doesn't work if nObject "hitDamage" parameter equals 0!
      */
      wormDestroy: true,

      /*
        How many blood particles (nObject6) should be created on worm hit, divided by 2.
        So, if you set it to "10", then 5 blood particles will be created on worm hit (per each frame - which means that the amount of blood particles is affected by "wormDestroy" parameter).
        Note: this property doesn't work if nObject "hitDamage" parameter equals 0!
      */
      bloodOnHit: 0,

      /*
        First sprite of animation used for nObject.
        If -1 or 0, it will be a single pixel using a colour indicated in:
        a) "splinterColour" property (used in in the other wObject or nObject which produces given nObject) - if this "splinterColour" property is set to 2 or more;
        b) "colorBullets" property - if "splinterColour" parameter mentioned above is set to 0 or 1.
      */
      startFrame: 165,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame". 
        Note: Animation begins on random frame, so is suitable really only for objects which have animation cycle which looks good regardless of what frame it starts. Think things like spinning grenades, mines, pulsing items, etc.
        Note: lack of "loopAnim" parameter for nObjects!
        Note: if numFrames > 0, then the animation cycle works only if the particle is moving; when the particle stops moving, the animation cycle also stops.
      */
      numFrames: 3,

      /* 
        When set to true, this makes the object to be drawn onto the map and object itself gets removed from the game.
        This is how mountains of shells and body parts are created in liero and also the reason why they clutter up the bunnyhoops.
        Note: works only on collsision with rock / dirt / edge of the map if explGround is set "true" and the object is no longer in motion!
      */
      drawOnMap: true,

      /*
        Color of the object. Works only if you use a pixel (startframe: -1 or 0) for a nObject and only if "splinterColour" parameter (used in in the other wObject or nObject which produced your nObject) is set to 0 or 1 (otherwise, your nObject will have color indicated in "splinterColour" parameter used in this other object).
      */
      colorBullets: 0,

      /*
        Which special object (sObject) to use on explosion. Those are stored in ordered arrays and are defined near the end of mod json file (counting started from 0). Set -1 for none.
      */
      createOnExp: 4,

      /*
        Whether the object is affected by explosions' push force (on collision with sObject).
        Note: works only if the colliding sObject has got detectRange > 0 and damage ≠ 0 and blowAway ≠ 0! 
      */
      affectByExplosions: false,

      /*
        Which dirt mask to use on object explosion (see "textures" part of json file). Set -1 for none.
      */
      dirtEffect: -1,

      /*
        Amount of nObjects to create on explosion. The object must actually explode, for example if "wormExplode" is set to false and "wormDestroy" is set to true, no nObjects will be created.
      */
      splinterAmount: 0,

      /*
        Color used on nObjects (produced as splinters) when they are a single pixel (startFrame -1 or 0). If splinterColour is set to 0 or 1, then splinters will have a colour indicated in nObject "colorBullets" property.
        Note: if splinterColour is set 2 or more, nObject splinter will actually use two colours: the one indicated in this parameter, and also the previous one. So, in this case, splinters will use colour 13 and 12.
      */
      splinterColour: 13,

      /*
        Type of nObjects to create when the object explodes. This refers to index of the nObject in the array (counting started from 0), so if you change the order of nObjects, something else will be used.
      */
      splinterType: -1,

      /*
        A nObject-specific property; when set to true, the nObject will trail blood particles.
        Note: when nObject stops moving in the air, it keeps creating blood particles on its trail. However, if nObject is spawned or "trapped" inside rock or dirt on the map, then it stops creating blood particles on its trail.
      */
      bloodTrail: true,

      /*
        Delay between blood particles.
      */
      bloodTrailDelay: 10,

      /* 
        sObject (special Object) used as a trail. Set -1 for none.
        Note: when nObject stops moving, it keeps creating sObjects on its trail. However, if nObject is spawned or "trapped" inside rock or dirt on the map, then it stops creating sObjects on its trail.
      */ 
      leaveObj: -1,

      /*
        Delay time (in frames) between creating trailing sObjects.
        Note: the delay is not referred to object's lifetime but game ticks, which means that it is measured in relation to the time elapsed since the game started, not since the object was created.
        Note: If "repeat" is set to greater than 1, sObjects will be created in "batches", creating intermittent lines instead of denser lines. This is probably a bug or unintended behavior.
        Note: notice that nObjects cannot trail other nObjects.
      */
      leaveObjDelay: 0,

      /*
        Time to explode in frames (the duration time of the object before it gets removed from the map). When set to 0 there will be no explosion at all. 
        Any positive value will cause creation of a designated sObject indicated in createOnExp parameter (if not -1).
        Note: it is not recommended to set negative value for this property; it might cause desynch issues for players who use WebLiero Extended hack and play on non-extended rooms.
        Note: it is not recommended to set timeToExplo > 32767, because it might cause desynch issues for players who join the room right after the object with such property value is created on the map.
      */
      timeToExplo: 8000,

      /*
        Maximum (negative) variation of time to explode in frames.
      */
      timeToExploV: 2000
    },
    // (...) all the other nObjects follow here
  ],  // End of nObjects descriptions' array
```
[[scroll to the top](#summary-)]

## sObjects 💥

`sObjects` are **special** Objects.

They are static objects which cannot be affected by anything, however they can affect other objects and worms. Their usual usage is to make explosions or non-movable trails.

Like other types of objects, they are indexed by their order in the array (counting started from 0).

```js
  sObjects: [
    {
      // large explosion sid0 - description of the sObject. This is just a comment with denoting the id of every sObject, same like I did with wObjects and nObjects.

      /*
        which sound (taken from soundpack) will be played when sObject is created; set -1 for no sound.
        startSound is first index used...
      */ 
      startSound: 9,

      /*
        ...and numSounds is amount of indices used to pick the starting sound from.
      */
      numSounds: 4,

      /*
        Delay time (in frames) before advancing to next frame during object animation.
      */
      animDelay: 2,

      /*
        First sprite of animation used for the object.
        Note: unlike for wObjects and nObjects, sObjects always start at proper startFrame.
        So if you set it to -1, weird things are gonna happen on sObject animation and the game can even freeze.
      */
      startFrame: 40,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame". 
      */
      numFrames: 15,

      /*
        Maximum range of the sObject when it begins to affect the worm.
        Note: if detectRange ⩽ 0, then the worm will not receive damage from the object and also blowAway parameter will not work.
        Note: if you set detectRange > 0, bonuses (weapon/health boxes) will still explode on a collision with such sObject even if its damage equals 0.
      */
      detectRange: 20,

      /*
        Damage applied to the worm if it's in explosion range (vide detectRange parameter).
        Note: this is affected by how far the worm is from the epicenter of the explosion (the position of sObject) - the closer the sObject is created to the worm, the more damage the worm will get; the further the sObject is created to the worm, the less damage the worm will get.
        Note: it is very rare for an explosion to be in exact point where the worm is. This means, usually the damage will be noticeably smaller than the number indicate in this parameter. Use about 2/3 of its value as a rough estimate of the damage usually given to a worm.
        Note: if you set negative value for it, you will have healing effect.
      */
      damage: 15,

      /*
        Force applied on the worm as pushback. Works only if detectRange > 0 and damage ≠ 0!
      */
      blowAway: 0.0457763671875,

      /*
        Whether the sObject should create a shadow. This is not implemented in WebLiero.
      */
      shadow: true,

      /*
        Duration of the screen shake effect caused by the sObject. This is not implemented in WebLiero.
      */
      shake: 4,

      /*
        Duration of the screen flash effect caused by the sObject. This is not implemented in WebLiero.
      */
      flash: 8,

      /*
        Which dirt mask to use on object creation (see "textures" part of json file). Set -1 for none.
      */
      dirtEffect: 0
    },
    // rest of sObjects follow here
  ], // End of sObjects descriptions' array
```
[[scroll to the top](#summary-)]

## Textures 🎨

These `textures` parameters has the reference in `nObject`, `wObject` and `sObject` as `dirtEffect`.

`dirtEffect` is a parameter present on all object types (`nObject`, `wObject` and `sObject`).

Changes here affects how all objects (wObjects, nObjects, sObjects) and worm (dirtEffect 0 and 7) interact with all materials on the map (especially with dirts).

Like other types of objects, they are indexed by their order in the array (counting started from 0).

Note: unlike in original Liero, in WebLiero there are no limits regarding size of sprites used for mFrame and sFrame, however if you want dirtEffects to work properly, then remember that sFrame sprite should always be no less than 16x16 pixels size.

```js
  textures: [  // dirt masks (dirt effects)
    {
      nDrawBack: true,  // causes Liero not to draw the anti-alias edges on the background. Normally turned "false" for creating dirt and rock & turned "true" for cleaning dirt.
      mFrame: 0,  // controls which sprite is used to cut a hole (= determines the size and shape of the hole).
      sFrame: 73,  // the texture the map change will leave behind (= which sprite is used to fill the hole).
      rFrame: 2  // the amount of sprites to use to fill the hole (starting from sFrame). Note: if you set 0 or 1, then only 1 sprite will be used to fill the hole (the one indicated in sFrame).
    },
   // rest of textures follow here
  ], // End of textures descriptions' array
```
[[scroll to the top](#summary-)]

## others 🐛

Other parameters which are usually defined in the last part of json file.

```js
  colorAnim: [129, 131, 133, 136, 152, 159, 168, 171],  // an array of colours (ID taken from the palette) which will be animated (colours will shine). This array actually specifies "subgroups" of colours to be animated. These "subgroups" are independent strings (sequences) of colours, where each one is determined by two factors, whereas the first one determines the beginning of the sequence and the next its end. So in this example there are 4 "subgroups" of animated colours: colours from 129 to 131, from 133 to 136, from 152 to 159, and from 168 to 171 (totally 19 animated colours). Note: you can add as many of such strings as you want.
  textSpritesStartIdx: 240,  // starting sprite for letters used for weapon names (which are displayed above the worm when you change weapons during game and above weapon crates). Note: no matter which sprite you set here, game will always take only 26 sprites for those letters, starting from sprite indicated in this parameter.
  crossHairSprite: 153,  // starting sprite for crosshair used by worm.
  name: "Promode ReRevisited", // name of the mod; will be displayed in the room options menu. If you don't use this function, the mod name in the room options menu will be displayed as "Unnamed Mod".
  author: "Scharnvirk-Jerac", // author of the mod.
  version: "1.3.2", // version of the mod.
  }  // End of the mod file 
```
[[scroll to the top](#summary-)]

## Extended 🧪

There are some new cool parameters implemented in WebLiero via [WebLiero Extended hack](https://www.vgm-quiz.com/dev/webliero/extended), which you can use in json file. They work only if you use WebLiero Extended.

- **noRope** _(boolean, section "constants")_ - disables rope when set "true"
- **removeOnSObject** _(boolean, for wObject)_ - if set "true", object will be removed on collision with sObject (like wObject34)
- **immutable** _(boolean, for wObject and nObject)_ - prevents object to be moved by other wObjects with "collideWithObjects" property set "true"
- **fixed** _(boolean, for wObject)_ - prevents object to be affected by its own velocity (object doesn't move even when its speed is > 0)
- **teamImmunity** _(int, for wObject and nObject)_ - defines which team players are immune to the given object (the object will not collide with and give damage to such players): 0 - spectators / 1 - team alpha (green) / 2 - team bravo (blue); in non-team game modes, all players are immune to objects with teamImmunity set to 1
- **detonable** _(boolean, for wObject)_ - if wObject explodes remotely by using a detonator key; works only if timeToExplo > 0
- **platform** _(boolean, for wObject)_ - makes object behave like a platform that you can hook rope into
- **platformWidth** _(int, for wObject)_ - sets platform width (size in X offset)
- **platformHeight** _(int, for wObject)_ - sets platform height (size in Y offset)
- **platformVelocityAuto** _(boolean, for wObject)_ - whether platform transfers its velocity to a worm
- **reloadSound** _(int, section "weapons")_ - sets custom sound for reload, none (-1) for default; currently does not work yet
- **overlay** _(boolean, for wObject)_ - if set "true", the object sprite will be drawn over a worm sprite
- **underlay** _(boolean, for wObject)_ - if set "true", the object sprite will be drawn below (under) all other sprites
- **beacon** _(boolean, for wObject)_ - if set "true", the object sprite will be invisible but displayed as a beacon (indicator) when the object is outside of the player's viewport (similar like the flag in HTF)

What is more, starting from WebLiero Extended version 72, a new crazy parameter has been added for wObject - **behaviors**. With behaviors it is possible to "programme" wObjects with custom logic in pure javascript, such as e.g. homing missiles, particles orbiting around the worm (or following the worm), objects sticking to worms, objects "attacking" closest worms etc. The same behavior can be associated to many wObjects, but a given wObject can have only one behavior. To learn more about behaviors, visit this [website](https://www.vgm-quiz.com/dev/webliero/extended-behaviors).

[[scroll to the top](#summary-)]

## WLSPRT ⛰️

The `WLSPRT` file format is a custom binary format used by WebLiero. It is a file which contains gfx part of the mod, i.e. sprites and palette.

There are many ways how to modify .wlsprt file, but there are two recommended programs which you can use for that purpose, i.e.:

1) [wledit](https://www.vgm-quiz.com/dev/webliero/wledit/) (works in browser - Internet connection needed - but it can be also downloaded on HD and then it works offline)
2) [wgetch's wltools](https://liero.phazon.xyz/wltools.html) (works only in Linux-type OS, no Internet connection needed)

In both aforementioned programs:

a) when you export sprites from .wlsprt file, then sprites will be presented as .png files which have got special names:

`XX_YYYY_ZZZZ.png`

whereas:

1. XX - natural number (0, 1, 2... etc.), representing the position of sprite in the spritesheet
2. YYYY - hexadecimal number, representing position of sprite center in X offset
3. ZZZZ - hexadecimal number, representing position of sprite center in Y offset

The game will read .wlsprt file and sprites correctly if you:

- make sprites using special palette (more info below);
- set properly X and Y offset for sprite center for every sprite;
- set properly position for every sprite in the spritesheet;
- include palette in the file.

**Few general assumptions regarding sprites:**

- sprites are stored in the spritesheet in an ordered array, starting from 0. So, if there are already e.g. 256 sprites and you want to add 1 new sprite, it shall be named 256_YYYY_ZZZZ.png
- the position of sprite in the spritesheet (XX) is important, because this is the way the game reads sprites, for example when you set startframe for weapon in json file (e.g. if you set startframe 110 for wObject, then the game will use sprite 110 for it)
- the sprite offsets (YYYY and ZZZZ parameters) affect the center (sometimes called sprite origin) of the sprite in-game. Offsets can be positive or negative and are not bounded by the sprite dimensions
- maximum size of whole spritesheet is 1024x1024
- no limits for size (dimensions) of single sprite and amount of sprites in WLSPRT file (but if the total dimensions of all sprites altogether are bigger than the maximum allowed size of the whole spritesheet, then iterating the sprites in the WLSPRT file may result in crashes, corruption or other bugs)
- as it was written before, there are some mandatory sprites for some objects in game which you can modify, but you cannot set different indices: bonuses, flag, hook for rope, worm, firecone (note: if you remove these sprites from .wlsprt file, especially worm sprites, the game will become buggy, can even freeze and generate console errors in the browser, so be aware of it)
- as it was written before, you cannot change crosshair position in X/Y offset, so even though in WebLiero it is possible to make bigger sprites for worms, it is not recommended to do it
- some sprites (3-8, 56-63, 69-72, 75, 85-86, 89-90, 96-98) are used on randomly-generated maps (as stones, bones etc.), so if you change them, it will have impact on how such maps will look (note: if you remove these sprites from .wlsprt file and try to load randomly generated map, the game will become buggy, can even freeze and generate console errors in the browser, so be aware of it)
- worm sprites are always displayed "on top" (sprites used by wObjects, nObjects and sObjects are always drawn "behind" the worm - except for sprites used in wObjects with "overlay" parameter set to "true" in WebLiero Extended; [see more info about it in the "Extended" section](#extended-)), which means that it's impossible to make a weapon which will work like invisibility cloak; some game objects are rendered "after" the worm though (crosshair, blood trail particles, firecone and letters used to display weapon names when you hold weapon change button), which means that they will cover worm sprite

b) palette is represented as one .png file named "palette.png" after exporting it from .wlsprt file. Palette defines what colours are used in the mod (note: it affects not only sprites but also maps).

**Few general assumptions regarding palette:**

- palette must be made in indexed mode, 256 colours (for more info about colours and their material properties - see this [material guide made by wgetch](https://liero.phazon.xyz/materials.html))
- currently, you cannot edit or create new palette in wledit or wgetch's wltools (you can only import / export whole palette). You can edit or create new palette in other ways though, for example by using older tools made for classic Liero, e.g. LieroKit (wledit can import .lpl files)
- you can use any colour you want to make sprites (but see info below about making sprites for worms and colour 0)
- you can modify the colours in palette in any way you want, but since palette uses indexed mode, each colour is strongly connected with material properties which cannot be modified in any way. This means that you cannot change the material properties of any colour ID (e.g. colour 19 is always rock) or the amount of any material type colours (i.e. there can be only 4 see shadow colours) - unless you use WebLiero Extended hack (where such modifications are possible). However, material properties and colour indices are important and have impact only when you:
a) make maps for WebLiero
b) use drawOnMap: true in nObject
c) use dirtEffects - in mFrame (see info below) and sFrame
- special properties for worm colours (30-38): you can modify worm colours (by changing palette), but it is not recommended to use different colour indices for worm sprites. If you use another colour indices for worm sprites, worms will not receive dmg from wObjects and nObjects (only from sObjects) + worms will not have custom colours that the player defines before entering the room
- colour index 0 is special: when you use it to make sprites, it will be represented as transparent in the game
- special colours used for mFrame in textures: to make dirtEffect work correctly, the "hole" in mFrame sprite must be made with colour ID 6 or 10 (for textures with nDrawBack parameter set to "false") or colour ID 6 (for textures with nDrawBack parameter set to "true"); if you also want to have anti-alias edges created, then those edges in mFrame sprite must be bade with colour ID 1 or 2 (for textures with nDrawBack parameter set to "false") or colour ID 1 (for textures with nDrawBack parameter set to "true")
- when you use worm colours (30-34) for objects which are rendered "after" the worm (crosshair, blood trail particles, firecone and letters used to display weapon names when you hold weapon change button), then those objects will change colours every time when any player spawns on the map (except for firecone, which will inherit the same colour as its "owner", i.e. when your worm is blue, then the firecone displayed when your worm shoots will be blue too during whole game; if your opponent's worm is red, then its firecone displayed when it shoots will be red too and so forth)
- in team game modes (e.g. team deathmatch), worms are always green (team alpha) or blue coloured (team beta), regardless of which color you set for the worm in player setup panel. What is more, worm indicators (the little crosses that show the positions of other players when they are out of screen range) in team game modes will also be green or blue (depending on the team the other players belong to). These colors (blue/green) cannot be changed neither by editing json nor wlsprt file. However, if you use colors other than 30-34 to draw worm sprites, then all worms in team game modes will have the same colour(s) - regardless of which team they are on (although worm indicators will still be green or blue respectively; by analogy, in non-team game modes those indicators will always have colors corresponding to those which were set before the start of the game in player setup panel on webliero main page, regardless of what colors worms sprites have in the game)

For more info about wlsprt file, [see this guide made by wgetch](https://liero.phazon.xyz/wlsprt.html).

[[scroll to the top](#summary-)]

## Credits 🤗

Special thanks to:

- basro, for creating the best game in the world :)
- Jerac, for making first version of this mod guide and for creating some really cool mods
- vitu, for adapting the mod guide for github with nice layout and for creating some cool mods
- Ophi, fo creating magnificent WebLiero Extended hack and fantastic wledit
- wgetch, for his cool liero pages, lots of epic WebLiero maps and for creating useful wltools
- dsds, for his contribution to Liero community (especially for hosting 90% of headless rooms and thus making this game still alive xD), his huge knowledge about WebLiero, scripting, as well as for making lots of epic maps for WebLiero, awesome scripts and mods
- Larcelo, for giving some insights on this mod guide and for creating some cools mods
- pilaf, for making lots of useful content regarding WebLiero on his github (WebLiero FAQ, instructions how to make WebLiero headless room, as well as for adapting liero palette for gimp)
- Daro and kami, for creating many great mods and helping discover the meaning and operation of many parameters in json file
- Piotr, for the hard work he put into searching for the perfect balance of weapons in mods and being the best WebLiero player of all time
- AlphaBlueWolf12, for being AlphaBlueWolf12 :)

[[scroll to the top](#summary-)]
